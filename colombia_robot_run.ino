//======================================================================
// prototype sketch
//
// all robot actions will go here
//======================================================================


#include <EEPROM.h>

#include "parser.h"
#include "drive_motors.h"
#include "arm_motor.h"
#include "line_sensor.h"
#include "dist_sensor.h"
#include "user_settings.h"

#include "robot_run.h"

#include "memory_free.h"


//======================================================================
// these are the global values needed for the various commands
// user_settings are saved to EEPROM when changed and restored on boot
// Te only thing we need to save for dist_vals is the threshold
//======================================================================

USER_SETTINGS_t user_settings;

DIST_SENSOR_VALS    dist_vals;

//======================================================================
// this is the table of user commands
// When the command is typed into the "send" box in the Serial Monitor,
// the table is searched for the command.
// When the command is found, the function in the PARSER_CALLBACK( function )
// is called. That function can then use the arguments for the action
//
// First item is the command name
// second item is the number of arguments it takes.
// third item is the name of the function to call.
//======================================================================

Command user_commands[] =
{
    //=============================================================
    // these are the driver motor commands
    //=============================================================

    {"drivespeed",  0,  PARSER_CALLBACK(cmd_drive_speed),
               "drivespeed [<speed>] - print speed or set speed 50..255"},

    //=============================================================
    // these are for the ARM motor
    //=============================================================

    {"armspeed",  0,  PARSER_CALLBACK(cmd_arm_speed),
               "armspeed [<speed>] - print arm speed or set speed 50..255"},

    //=============================================================
    // these commands are for the line sensor
    //=============================================================

    {"lineread",    0,  PARSER_CALLBACK(cmd_line_sensor_read),
              "lineread - read the line sensors"},

    //=============================================================
    // these are for the distance sensor
    //=============================================================

    {"distread",    0,  PARSER_CALLBACK(cmd_dist_sensor_read),
              "distread - read the dist sensors"},

    {"distthresh",    0,  PARSER_CALLBACK(cmd_dist_sensor_thresh),
     "distthresh - set the distance threshold"},

    //=============================================================
    // this starts and stops the line following algorithm
    //=============================================================

    {"start",  0,  PARSER_CALLBACK(cmd_start_robot),   "start the robot"},
    {"stop",   0,  PARSER_CALLBACK(cmd_stop_robot),    "stop the robot"},

    //=============================================================
    // This tells us we have hit the end of our command list.
    // Don't touch this. Don't put anything after this (it will be ignored!)
    //=============================================================

    {null,-1,null,null}
};


//======================================================================
// The Serial.begin(9600) line must be in the setup() function
//======================================================================

void setup()
{
    //===================================================================
    // the serial port is used for the commands thru the Arduino serial monitor
    //===================================================================

    Serial.begin(9600);

    Serial.println( "Starting the serial command line" );

    //===================================================================
    // get the values from EEPROM
    // we read into user_settings and check the type
    // if the type is bad, we fill the struct and write it out
    //
    // these functions are at the end of this file
    //===================================================================

    eeprom_read_user_settings();
    if( user_settings.type != TYPE_USER_SETTINGS )
    {
        eeprom_fill_user_settings();
        eeprom_write_user_settings();
    }

    //===================================================================
    // the drive motors make the robot move forward, back, left, right
    //===================================================================

    drive_motors_init();

    //===================================================================
    // The ARM motor is for the ping-pong ball magazine
    //===================================================================

    arm_motor_init();

    //===================================================================
    // the line sensors are for the line-follower
    //===================================================================

    line_sensor_init();

    //===================================================================
    // the distance sensor is the distance to the box
    //===================================================================

    dist_sensor_init( &dist_vals );
    dist_vals.thresh = user_settings.dist_thresh;

}

//======================================================================
// this functio fills the user_settings areuct with the default values
//======================================================================

void eeprom_fill_user_settings()
{
    user_settings.type           = TYPE_USER_SETTINGS;
    user_settings.dist_thresh    = DIST_SENSOR_THRESH;
    user_settings.user_speed     = MOTOR_MAX_SPEED;
    user_settings.arm_user_speed = ARM_MOTOR_MAX_SPEED;
}

//======================================================================
// These are the callback functions for the drive motors
// they are called by the parser for a specific command
//======================================================================
//
// cmd_drive_speed()
//
// set the speed from 0 to 255.
// 0 is full stop (no motion at all for any command) and 255 is full speed
// we will make the minimum speed that can be set at 50 - really slow,
// but the robot will move
//======================================================================

void cmd_drive_speed( ARGS *arg_list )
{
    //====================================================================
    // if 0 arguments, then print out the drivespeed
    //====================================================================

    if( arg_list->num_arguments == 0 )
    {
        Serial.print( "drivespeed = " );
        Serial.println( user_settings.user_speed );
        return;
    }

    //====================================================================
    // if an argument was given, it must be one number between
    // MOTOR_MIN_USER_SPEED and MOTOR_MAX_SPEED
    //====================================================================

    if(  (arg_list->num_arguments != 1)
      || (arg_list->values[ 0 ] < MOTOR_MIN_USER_SPEED)
      || (arg_list->values[ 0 ] > MOTOR_MAX_SPEED) )
    {
        Serial.print( "drivespeed must be a speed " );
        Serial.print( MOTOR_MIN_USER_SPEED );
        Serial.print( " to " );
        Serial.println( MOTOR_MAX_SPEED );
        return;
    }

    //====================================================================
    // start the motion, delay values[0] second (1000 milliSec), stop motor
    //====================================================================

    user_settings.user_speed = arg_list->values[ 0 ];
    eeprom_write_user_settings();

    Serial.print( "drivespeed = " );
    Serial.println( user_settings.user_speed );
}


//======================================================================
// These are the callback functions for the ARM motor
// they are called by the parser for a specific command
//======================================================================
//
// cmd_arm_speed()
//
// set the speed from 0 to 255.
// 0 is full stop (no motion at all for any command) and 255 is full speed
// we will make the minimum speed that can be set at 50 - really slow,
// but the robot will move
//======================================================================

void cmd_arm_speed( ARGS *arg_list )
{
    //====================================================================
    // if 0 arguments, then print out the armspeed
    //====================================================================

    if( arg_list->num_arguments == 0 )
    {
        Serial.print( "armspeed = " );
        Serial.println( user_settings.arm_user_speed );
        return;
    }

    //====================================================================
    // if an argument was given, it must be one number between
    // ARM_MOTOR_MIN_USER_SPEED and ARM_MOTOR_MAX_SPEED
    //====================================================================

    if(  (arg_list->num_arguments != 1)
      || (arg_list->values[ 0 ] < ARM_MOTOR_MIN_USER_SPEED)
      || (arg_list->values[ 0 ] > ARM_MOTOR_MAX_SPEED) )
    {
        Serial.print( "armspeed must be a speed " );
        Serial.print( ARM_MOTOR_MIN_USER_SPEED );
        Serial.print( " to " );
        Serial.println( ARM_MOTOR_MAX_SPEED );
        return;
    }

    //====================================================================
    // start the motion, delay values[0] second (1000 milliSec), stop motor
    //====================================================================

    user_settings.arm_user_speed = arg_list->values[ 0 ];
    eeprom_write_user_settings();

    Serial.print( "armspeed = " );
    Serial.println( user_settings.arm_user_speed );
}



//======================================================================
// These are the callback functions for the line sensor
//======================================================================

void cmd_line_sensor_read( ARGS *arg_list )
{
    //====================================================================
    // this is where the values are put
    // the variable is used only when this function is called,
    // so we can define it here (locally) instead of globally
    //====================================================================

    LINE_SENSOR_VALS  line_vals;

    //====================================================================
    // do the read and print out the values
    //====================================================================

    line_sensor_read( &line_vals );
    
    Serial.print( "left = " );
    Serial.print( line_vals.left_sensor );

    Serial.print( "; right = " );
    Serial.print( line_vals.right_sensor );

    Serial.println( "  " );
}



//======================================================================
// These are the callback functions for the distance sensor
// they are called by the parser for a specific command
//======================================================================

void cmd_dist_sensor_thresh( ARGS *arg_list )
{
    //==================================================================
    // no arguments: reset to the threshold - really re-do the init
    //==================================================================

    if( arg_list->num_arguments == 0 )
    {
        dist_sensor_init( &dist_vals );
        eeprom_write_user_settings();
        Serial.println( "reset the distance values" );
        return;
    }

    //==================================================================
    // check for one arg, and the value is less than the max
    //==================================================================

    if(  (arg_list->num_arguments != 1 )
      || (arg_list->values[ 0 ] > DIST_SENSOR_THRESH_MAX) )
    {
        Serial.print( "Need a threshold <= " );
        Serial.print( DIST_SENSOR_THRESH_MAX );
        Serial.println( "" );
        return;
    }

    //==================================================================
    // set the new thresh
    //==================================================================

    dist_sensor_thresh( &dist_vals, arg_list->values[ 0 ] );
    eeprom_write_user_settings();

    Serial.print( "dist threshold = " );
    Serial.print( dist_vals.thresh );
    Serial.println( "  " );
}

//======================================================================
// read the sensor
//======================================================================

void cmd_dist_sensor_read( ARGS *arg_list )
{
    //====================================================================
    // do the read and print out the values
    //====================================================================

    dist_sensor_read( &dist_vals );

    Serial.print( "dist = " );
    Serial.print( dist_vals.at_box );
#if 1
    Serial.print( "  (" );
    Serial.print( dist_vals.dist );
    Serial.print( ")" );
#endif

    Serial.println( "  " );
}

//======================================================================
// start and stop the robot actions of following the line
//======================================================================

void cmd_start_robot( ARGS *arg_list )
{
    robot_run_start();
}

void cmd_stop_robot( ARGS *arg_list )
{
    robot_run_stop();

}


//======================================================================
// "run_parser();" must be in the loop() function
//======================================================================

void loop()
{
    byte    ret;

    //==================================================================
    // detect a line from the Arduino serial monitor
    // (NOTE: Must have the "line feed" selected in the lower-right corner
    // of the window, and type in the single bar at the top of the window)
    //==================================================================

    if( Serial.available() )
        run_parser();

    memory_free_report();
  
    //==================================================================
    // this is the robot following the line
    //==================================================================

    robot_run();

}

//======================================================================
// the eeprom functions
// we use the EEPROM library
// The trick here is we create a byte pointer to the start of user_settings
// and treat it like a byte array, length = sizeof(user_settings)
//======================================================================

void eeprom_read_user_settings()
{
    byte *ptr;
    int  i;

    ptr = (byte *) &user_settings;
    
    for( i = 0; i < sizeof(user_settings); i++ )
    {
        ptr[ i ] = EEPROM.read( i );
    }
}

//======================================================================
// write the current values in user_settings to the EEPROM
//======================================================================

void eeprom_write_user_settings()
{
    byte *ptr;
    int  i;

    ptr = (byte *) &user_settings;
    
    for( i = 0; i < sizeof(user_settings); i++ )
    {
        EEPROM.write( i, ptr[ i ] );
    }
}

//======================================================================
//======================================================================


