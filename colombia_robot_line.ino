//======================================================================
// prototype sketch - LINE_SENSOR example
//
// all robot actions will go here
//======================================================================


#include "parser.h"
#include "line_sensor.h"
#include "memory_free.h"


//======================================================================
// this is the table of user commands
// When the command is typed into the "send" box in the Serial Monitor,
// the table is searched for the command.
// When the command is found, the function in the PARSER_CALLBACK( function )
// is called. That function can then use the arguments for the action
//======================================================================

Command user_commands[] =
{
    //=============================================================
    // First item is the command name
    // second item is the number of arguments it takes.
    // third item is the name of the function to call.
    //=============================================================

    {"lineread",    0,  PARSER_CALLBACK(cmd_line_sensor_read),
              "lineread - read the line sensors"},

    //=============================================================
    // This tells us we have hit the end of our command list.
    // Don't touch this. Don't put anything after this (it will be ignored!)
    //=============================================================

    {null,-1,null,null}
};


//======================================================================
// The Serial.begin(9600) line must be in the setup() function
//======================================================================

void setup()
{
    Serial.begin(9600);

    //===================================================================
    // the arm motor make the robot move forward, back, left, right
    //===================================================================

    line_sensor_init();
}


//======================================================================
// These are the command callback functions
// they are called by the parser for a specific command
//======================================================================

void cmd_line_sensor_read( ARGS *arg_list )
{
    //====================================================================
    // this is where the values are put
    //====================================================================

    LINE_SENSOR_VALS  line_vals;

    //====================================================================
    // do the read and print out the values
    //====================================================================

    line_sensor_read( &line_vals );
    
    Serial.print( "left = " );
    Serial.print( line_vals.left_sensor );

    Serial.print( "; right = " );
    Serial.print( line_vals.right_sensor );

    Serial.println( "  " );
}


//======================================================================
// "run_parser();" must be in the loop() function
//======================================================================

void loop()
{
    if( Serial.available() )
        run_parser();
  
    memory_free_report();
}

