//======================================================================
// dist_sensor.h
//
// These are the functions and defines needed for dist_sensor.cpp
//
// void dist_sensor_init()
// void dist_sensor_read( DIST_SENSOR_VALS *vals );
//
// There are two GP2Y0D810Z0F sensors - one on the left and one on the right.
// they want to return 0 for white
//======================================================================

#ifndef _DIST_SENSOR_H_
#define _DIST_SENSOR_H_

//======================================================================
// this is the return from the dist_sensor_read() function
// the values will be 0..1023
//======================================================================

typedef struct
{
    boolean         at_box;

#define DIST_SENSOR_NUM_AVE     16
#define DIST_SENSOR_THRESH      (950)      // below: too far; above: at box
#define DIST_SENSOR_THRESH_MAX  (1000)     // max threshold (really 1023)

    unsigned int     dist_ave[ DIST_SENSOR_NUM_AVE ];
    unsigned int     dist;

    unsigned int     thresh;

} DIST_SENSOR_VALS;

extern DIST_SENSOR_VALS     dist_vals;

//======================================================================

void dist_sensor_init( DIST_SENSOR_VALS *vals );
void dist_sensor_thresh( DIST_SENSOR_VALS *vals, unsigned int thresh );
void dist_sensor_read( DIST_SENSOR_VALS *vals );

#endif // _DIST_SENSOR_H_
