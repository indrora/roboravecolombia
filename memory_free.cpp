//======================================================================
// memory_free.cpp free memory functions
// 
//
// return the ammount of free RAM
// code taken from the "Arduino Cookbook" by O'Reilly press
//
// The RAM usage should not change because the heap is not used.
// The critical thing is there is enough room for the stack for all cases
//======================================================================

#include <Arduino.h>
#include "memory_free.h"

//======================================================================
// thesee are the variables set by the compiler for the end of BSS
// and the start of the current heap
//======================================================================

extern int __bss_end;
extern void *__brkval;

//======================================================================
// called in the loop() function to report changes to RAM
//======================================================================

void memory_free_report()
{
    static int      mem_free = 2048;    // the amount of RAM in the chip
    int             val;

    //==================================================================
    // see if the RAM usage has changed
    //==================================================================

    val = memory_free();
    if( mem_free > val )
    {
        mem_free = val;     // new low RAM value
        Serial.print( "RAM usage (free bytes): " );
        Serial.print( mem_free );
        Serial.print( "; val = " );
        Serial.print( val );
        Serial.println( "" );
    }
}


//======================================================================
// int memory_free()
//
// return the amount of free RAM
// assumes a pro328 with 2K of RAM
//======================================================================

int memory_free()
{
    int free_val;

    if( (int) __brkval == 0 )
        free_val = ((int) &free_val) - ((int) &__bss_end);  //stack end to BSS
    else
        free_val = ((int) &free_val) - ((int)__brkval);      // stack to heap
    return( free_val );
}

