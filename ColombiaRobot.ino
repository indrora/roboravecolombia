//======================================================================
// prototype sketch
//
// all robot actions will go here
//======================================================================


#include "parser.h"



//======================================================================
// this is the table of user commands
// When the command is typed into the "send" box in the Serial Monitor,
// the table is searched for the command.
// When the command is found, the function in the PARSER_CALLBACK( function )
// is called. That function can then use the arguments for the action
//======================================================================

Command user_commands[] =
{
  //=============================================================
  // First item is the command name
  // second item is the number of arguments it takes.
  // third item is the name of the function to call.
  //=============================================================

  {"add",  2,  PARSER_CALLBACK(cmd_add), "Add two numbers"},
  {"sub",  2,  PARSER_CALLBACK(cmd_sub), "Subtract two numbers"},
  {"time", 0,  PARSER_CALLBACK(cmd_time), "System uptime"},

  //=============================================================
  // This tells us we have hit the end of our command list.
  // Don't touch this. Don't put anything after this (it will be ignored!)
  //=============================================================

  {null,-1,null,null}
};


//======================================================================
// The Serial.begin(9600) line must be in the setup() function
//======================================================================

void setup()
{
  Serial.begin(9600);
}





//======================================================================
// Some demonstration commands.
//
// These are the functions used in the PARSER_CALLBACK( function )
// part of the table entries
// Notice how the functions are defined:
//
//     void function ( ARGS *arg_list )
//
// "void" means the function does something, but does not return a value
// "function" is the name of the function
// "ARGS *arg_list" is a pointer to a structure in parser_types.h
// 
//     typedef struct
//     {
//       byte num_arguments;
//       int  values[MAX_ARGUMENT_COUNT];
//     } ARGS;
//
// If you type the command "add 5 6" the function "cmd_add()" gets a pointer
// to a filled in structure ARGS, named arg_list.
// To use:
//     arg_list->num_arguments      = number of arguments
//     arg_list->values[0]          = first number after the command
//     arg_list->values[1]          = second number after the command
//     arg_list->values[2]          = etc
// Currently, you can have 0 to 8 arguments (numbers) after the command
// So, for the example "add 5 6":
//     arg_list->num_arguments      = 2
//     arg_list->values[0]          = 5
//     arg_list->values[1]          = 6
//     arg_list->values[2]          = not used
//
// Notice you can ignore all arguments if the function does not need any
//======================================================================

void cmd_add( ARGS *arg_list )
{
  if( arg_list->num_arguments != 2 )
  {
    Serial.println( "add needs two arguments" );
  }

  Serial.println(arg_list->values[0] + arg_list->values[1]);
}

void cmd_sub( ARGS *arg_list )
{
  if( arg_list->num_arguments != 2 )
  {
    Serial.println( "sub needs two arguments" );
  }

  Serial.println(arg_list->values[0] - arg_list->values[1]);
}

void cmd_time( ARGS* arg_list )
{
  Serial.print("I have been on for ");
  Serial.print(millis());
  Serial.println("ms");
}



//======================================================================
// "run_parser();" must be in the loop() function
//======================================================================

void loop()
{
    run_parser();
  
}
