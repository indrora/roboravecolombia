//=============================================================================
// parser_support.h
//
// Parser support package. This defines a header that we use for platform
// specific stuff. Don't worry if this looks really simple here. The complexity
// comes in parser_support.cpp
//=============================================================================

#ifndef _PARSER_SUPPORT_H_
#define _PARSER_SUPPORT_H_

//=======================================================================
// ASCII character ranges
//=======================================================================

#define ASCII_0 0x30
#define ASCII_9 0x39
#define ASCII_NEG 0x2D

#define ASCII_BKSP 0x08
#define ASCII_DEL  0x7F
#define ASCII_NL   0x0A
#define ASCII_CR   0x0D
#define ASCII_BEL  0x07
#define ASCII_SPACE  0x20

#define ASCII_PRINT_LOWER 32
#define ASCII_PRINT_UPPER 126


//=============================================================================
// function declaration
//=============================================================================

void PARSER_get_line(char*,int);
void PARSER_put_line(const char*);
void PARSER_put_format(const char* fmt, ...);
void PARSER_reset_line( boolean reset_rep, char *buf, unsigned int buf_len );

#endif//_PARSER_SUPPORT_H_

