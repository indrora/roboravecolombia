//======================================================================
// user_settings.h
//
// these are the user-settable values
// they are put into a structure/variable to make it easy to save and restore
// from EEPROM
//======================================================================

#ifndef _USER_SETTINGS_H_
#define _USER_SETTINGS_H_

//======================================================================

typedef struct
{
    //======================================================================
    // we need a type in the struct to determine if what
    // we read from EEPROM is valid, or we need to init it first
    // the value doesn't really matter - it just needs to be unsigned 16 bits
    //======================================================================

#define TYPE_USER_SETTINGS  0xBEEF

    unsigned int        type;

    //======================================================================
    // this is the user-settable speed
    // see drive_motor_speed()
    // we initialize it to the max speed
    //  byte user_speed = MOTOR_MAX_SPEED;
    //======================================================================

    byte                user_speed;

    //======================================================================
    // this is the user-settable speed
    // see arm_motor_speed()
    // we initialize it to the max speed
    //  byte arm_user_speed = ARM_MOTOR_MAX_SPEED;
    //======================================================================

    byte                arm_user_speed;

    //======================================================================
    // this is used for the distance sensor
    // the main thing we save is the threshold
    //======================================================================

    unsigned int        dist_thresh;

} USER_SETTINGS_t;

//======================================================================
// the external definition of the variable
//======================================================================

extern USER_SETTINGS_t  user_settings;

//======================================================================


#endif // _USER_SETTINGS_H_

