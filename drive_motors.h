//======================================================================
// drive_motors.h
//
// These are the functions and defines needed for drive_motors.cpp
//
// void drive_motors_init()
// void drive_motors_stop()
// void drive_motors_fwd( byte speed )
// void drive_motors_back( byte speed )
// void drive_motors_left( byte speed )
// void drive_motors_right( byte speed )
// void drive_motors_spin_left( byte speed )
// void drive_motors_spin_right( byte speed )
//
// The drive motors are connected to the H-bridge chip
// Change the #defines to the proper PWM outputs on the Arduino
// This mapping allows the motors to be connected to the pins
// and changed if needed
// Each motor has two PWM pins: called A and B
// (we need names, and these will work)
// remember the left and right motors rotate in OPPOSITE directions
// to make the robot move in the same direction
//======================================================================
// The basic motor circuit is:
//
//    [H-bridge "A"] ----- <motor> ----- [H-bridge "B"]
//
// both A and B are set to the minimum level by drive_motors_init()
// to make the motor turn in one direction, keep A at MIN_SPEED,
// and set B at MAX_SPEED. To make the motor turn in the opposite direction,
// set A at MAX_SPEED and B at MIN_SPEED
//======================================================================

#ifndef _DRIVE_MOTORS_H_
#define _DRIVE_MOTORS_H_

//======================================================================
// The motor speed is controlled by writing an analog value 0..255
//======================================================================

#define MOTOR_MIN_SPEED     0
#define MOTOR_MAX_SPEED     255

#define MOTOR_MIN_USER_SPEED    50  // minimum user can set the speed

//======================================================================

void drive_motors_init();
void drive_motors_stop();
void drive_motors_fwd( byte speed );
void drive_motors_back( byte speed );
void drive_motors_left( byte speed );
void drive_motors_right( byte speed );
void drive_motors_spin_left( byte speed );
void drive_motors_spin_right( byte speed );

#endif // _DRIVE_MOTORS_H_
