//======================================================================
// drive_motors.cpp
//
// These are the functions needed for the drive motors
//
// void drive_motors_init()
// void drive_motors_stop()
// void drive_motors_fwd( byte speed )
// void drive_motors_back( byte speed )
// void drive_motors_left( byte speed )
// void drive_motors_right( byte speed )
// void drive_motors_spin_left( byte speed )
// void drive_motors_spin_right( byte speed )
//
// The drive motors are connected to the H-bridge chip
// Change the #defines to the proper PWM outputs on the Arduino
// This mapping allows the motors to be connected to the pins
// and changed if needed
// Each motor has two PWM pins: called A and B
// (we need names, and these will work)
// remember the left and right motors rotate in OPPOSITE directions
// to make the robot move in the same direction
//======================================================================

#include <Arduino.h>
#include "robot_pins.h"
#include "drive_motors.h"

//======================================================================

void drive_motors_init()
{
  //====================================================================
  // set all PWM Motor pins to OUTPUT
  //====================================================================

  pinMode( LEFT_MOTOR_A, OUTPUT );
  pinMode( LEFT_MOTOR_B, OUTPUT );
  pinMode( LEFT_MOTOR_ENABLE, OUTPUT );

  pinMode( RIGHT_MOTOR_A, OUTPUT );
  pinMode( RIGHT_MOTOR_B, OUTPUT );
  pinMode( RIGHT_MOTOR_ENABLE, OUTPUT );

  //====================================================================
  // set all H-bridge outputs to the MINIMUM value
  //====================================================================

  drive_motors_stop();
}

//======================================================================
// The basic motor circuit is:
//
//    [H-bridge "A"] ----- <motor> ----- [H-bridge "B"]
//
// both A and B are set to the minimum level by drive_motors_init()
// to make the motor turn in one direction, keep A at MIN_SPEED,
// and set B at MAX_SPEED. To make the motor turn in the opposite direction,
// set A at MAX_SPEED and B at MIN_SPEED
//
// later .. by seetting to MIN speed, the battery is connected to ground
// via a resistor in the h-bridge, draining the battery
// the real fix is to add three lines to explicitly enable the channels
//======================================================================

void drive_motors_stop()
{
  digitalWrite( LEFT_MOTOR_ENABLE, LOW );
  analogWrite( LEFT_MOTOR_A, MOTOR_MIN_SPEED );
  analogWrite( LEFT_MOTOR_B, MOTOR_MIN_SPEED );

  digitalWrite( RIGHT_MOTOR_ENABLE, LOW );
  analogWrite( RIGHT_MOTOR_A, MOTOR_MIN_SPEED );
  analogWrite( RIGHT_MOTOR_B, MOTOR_MIN_SPEED );
}

//======================================================================

void drive_motors_fwd( byte speed )
{
  digitalWrite( LEFT_MOTOR_ENABLE, HIGH );
  analogWrite( LEFT_MOTOR_A, speed );

  digitalWrite( RIGHT_MOTOR_ENABLE, HIGH );
  analogWrite( RIGHT_MOTOR_A, speed );
}

//======================================================================

void drive_motors_back( byte speed )
{
  digitalWrite( LEFT_MOTOR_ENABLE, HIGH );
  analogWrite( LEFT_MOTOR_B, speed );

  digitalWrite( RIGHT_MOTOR_ENABLE, HIGH );
  analogWrite( RIGHT_MOTOR_B, speed );
}

//======================================================================

void drive_motors_left( byte speed )
{
  digitalWrite( LEFT_MOTOR_ENABLE, HIGH );
  analogWrite( LEFT_MOTOR_A, speed );
}

//======================================================================

void drive_motors_right( byte speed )
{
  digitalWrite( RIGHT_MOTOR_ENABLE, HIGH );
  analogWrite( RIGHT_MOTOR_A, speed );
}

//======================================================================

void drive_motors_spin_left( byte speed )
{
  digitalWrite( LEFT_MOTOR_ENABLE, HIGH );
  analogWrite( LEFT_MOTOR_A, speed );

  digitalWrite( RIGHT_MOTOR_ENABLE, HIGH );
  analogWrite( RIGHT_MOTOR_B, speed );
}

//======================================================================

void drive_motors_spin_right( byte speed )
{
  digitalWrite( RIGHT_MOTOR_ENABLE, HIGH );
  analogWrite( RIGHT_MOTOR_A, speed );

  digitalWrite( LEFT_MOTOR_ENABLE, HIGH );
  analogWrite( LEFT_MOTOR_B, speed );
}



