//=======================================================================
// Parser: a common parser for the Arduino
// 
// TODO:
// 1. in run_parser(), return if no input found
//=======================================================================

#include "parser.h"
#include "parser_types.h"
#include "parser_support.h"
#include "memory_free.h"

#include <Arduino.h>
#include <Platform.h>

//=======================================================================
// local function forward declarations
//=======================================================================

static int find_command(Command *cmd_set , char *cmd_name, Command * cmd);

static void PARSER_set_rep(ARGS *arguments);
static void PARSER_show_help(ARGS *arguments);
static void PARSER_dump_help(Command *cmd_set);

//=======================================================================
// these are the global commands
// this table is not changed by the user
//=======================================================================

static Command internal_commands[] =
{
  {"rep", 0, PARSER_CALLBACK(PARSER_set_rep), "Set repeat counter"},
  {"help",0, PARSER_CALLBACK(PARSER_show_help), "show this help"},
  {"?",0, PARSER_CALLBACK(PARSER_show_help), "show this help"},

  {null, 0, null, ""}      // table delimiter
};

//=======================================================================
// Some things we need to have
// CMD_BUFFER_SIZE defines how much we can (at max) read at a time.
// it is set to a massive size here to make things "easy"
//=======================================================================

#define CMD_BUFFER_SIZE 40

//=======================================================================
// the parser buffers
// note these are set = 0 on boot by the boot code
//=======================================================================

static char buffer[CMD_BUFFER_SIZE];
static unsigned int buff_length = 0;

static ARGS arguments = { 0,{0} };

static Command cmd = { null,0,null,null };

//=======================================================================
// How many times we should run any given command.
//=======================================================================

int rep_times = 1;
boolean rep_forever = false;
boolean rep_just_called = false;
boolean stop_repeat_flag = false;


//=======================================================================
// utility function for user command loops
// calling sequence:
//      while( in a loop )
//      {
//          if( stop_repeat() )
//              break;
//          // otherwise continue the user loop
//      }
//
// TODO: CLEANUP - FIXME - gotta be a cleaner way to do this
//=======================================================================

boolean stop_repeat()
{
    if(Serial.peek() == '!')
    {
      Serial.read(); // eat the !
      PARSER_reset_line( true, buffer, sizeof(buffer) );
      stop_repeat_flag = true;
    }
    return( stop_repeat_flag );
}

//=======================================================================
// void run_parser()
//
// this is the public function that gets called by the loop() function
//=======================================================================

void run_parser()
{
  //====================================================================
  // buffer to keep things sane.
  //
  // TODO: return if no input
  //====================================================================

  while(buffer[0] == 0x00)
  {
    //==================================================================
    // if no serial data, get out now.
    //==================================================================

    PARSER_get_line(buffer, CMD_BUFFER_SIZE);  
  }

  //==================================================================
  // change all ' ' to 0x00 past this.
  // This means we can now treat each argument as its own string.
  // We can then parse our values out better.
  //==================================================================

  buff_length = strlen( buffer );

  for( unsigned int alter_idx = 0; alter_idx< buff_length; alter_idx++ )
  {
    if( buffer[ alter_idx ] == ASCII_SPACE )
      buffer[ alter_idx ] = 0x00; 
  }

  //==================================================================
  // search the ingternal and user tables for the command
  // buffer[0] is the start of the command string
  // this is a 0x00 terminated string from the above 'set all  spaces to 0x00'
  //==================================================================

  if( 0 != find_command( internal_commands, buffer, &cmd) )
  {
    //==================================================================
    // check if it is a command the user gave.
    //==================================================================
      
    if( 0 != find_command(user_commands, buffer, &cmd) )
    {
      PARSER_put_format( "Unknown command %s\r\n", buffer );
      PARSER_reset_line( true, buffer, sizeof(buffer) );
      return;
    }
  }
    
  //==================================================================
  // Find all our integer arguments.
  // remember, all of the spaces are now 0x00
  // so strlen(buffer) finds the first 0x00 in the input buffer past
  // the first string (the command name)
  //==================================================================

  int arg_idx = 0;
  unsigned int arg_off = strlen( buffer );
    
  //==================================================================
  // search thru the rest of the buffer for the arguments
  // remember - we can have multiple 0x00 in a row
  // also - want to stop when run out of the original buffer length
  //==================================================================

  while(arg_off < buff_length)
  {
    if( buffer[ arg_off ] == 0x00 )
    {
      arg_off++;    // advance past 0x00
      continue;
    }
      
    arguments.values[arg_idx] = atoi(&(buffer[arg_off]));
    arg_off += strlen(&buffer[arg_off]);
    arg_idx++;
  }
    
  //==================================================================
  // set the total number of arguments and check the minimum number of args
  //==================================================================

  arguments.num_arguments = arg_idx;
  if( arguments.num_arguments < cmd.min_operand_count )
  {
    PARSER_put_format("Too few arguments for command %s\r\n", cmd.opcode);
    PARSER_reset_line( true, buffer, sizeof(buffer) );
    return;
  }
    
  //==================================================================
  // execute the command:
  //    once (normal case)
  //    rep times
  //    continuously until told stop
  //==================================================================

  stop_repeat_flag = false;

  while( rep_forever || (rep_times > 0) )
  {
    if(Serial.peek() == '!')
    {
      Serial.read(); // eat the !
      PARSER_reset_line( true, buffer, sizeof(buffer) );
      stop_repeat_flag = true;
      break;
    }

    //Serial.println( "callback" );
    PARSER_CALLBACK(cmd.callback)(&arguments);

    if( rep_just_called )   // want to reset the cmd line, but not the rep count
    {
      Serial.println( "found REP" );
      PARSER_reset_line( false, buffer, sizeof(buffer) );
      return;
    }

    if( !rep_forever && (rep_times > 0) )
      rep_times--;

    //Serial.print( "rep = " );
    //Serial.println( rep_times );
  }

  //===================================================================
  // all done - start with fresh buffer
  //===================================================================

  PARSER_reset_line( true, buffer, sizeof(buffer) );
}


//=======================================================================
// Finds the command in find_it. If not found, return 1.
// Otherwise, return 0.
//=======================================================================

static int find_command(Command *cmd_set, char *cmd_name, Command *cmd)
{
  //=====================================================================
  // Run through table until we find the command we want
  // or we run out of table.
  //=====================================================================

  for(int idx = 0; (cmd_set[ idx ].opcode) != null; idx++)
  {
    if( strcmp( cmd_name, cmd_set[idx].opcode ) == 0 )
    {
      *cmd = cmd_set[idx];      // this is a STRUCT copy
      return( 0 );
    }
  }
  //cmd = null;
  return( 1 );
}


//=======================================================================
//=======================================================================

static void PARSER_set_rep(ARGS *arguments)
{
  //=====================================================================
  // set our argument count
  //=====================================================================

  if(arguments->num_arguments > 0)
  {
    rep_forever = false;
    rep_times = arguments->values[0];
  }
  else
  {
    rep_forever = true;
    rep_times = 1;
  }

  rep_just_called = true;

}


//=======================================================================
//=======================================================================

static void PARSER_show_help(ARGS *arguments)
{    
    memory_free_report();
    PARSER_put_line("internal commands:");
    PARSER_dump_help(internal_commands);
    PARSER_put_line("defined commands:");
    PARSER_dump_help(user_commands);
    
}


//=======================================================================
//=======================================================================

static void PARSER_dump_help(Command *cmd_set)
{
   PARSER_put_line("function [args]\t help");

   for(int cmd_idx = 0; cmd_set[cmd_idx].opcode != null; cmd_idx++)
   {
     PARSER_put_format(" %s [%d]\t%s\r\n",
        cmd_set[cmd_idx].opcode,
        cmd_set[cmd_idx].min_operand_count,
        cmd_set[cmd_idx].help_text);
   }
}



