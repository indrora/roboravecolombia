//======================================================================
// robot_run.h - see robot_run.cpp
//
//======================================================================

#ifndef _ROBOT_RUN_H_
#define _ROBOT_RUN_H_

//======================================================================
// a general flag to start and stop the robot action
// stopping will reset to the start
//======================================================================

extern byte robot_running;  // set by the CLI

//======================================================================
//======================================================================

#define DUMP_HOPPER_MOTOR_DELAY     200     // mSec : TODO: make user_setting
#define DUMP_HOPPER_WAIT_SEC        5       // wait for PP to empty

//======================================================================
// this is the primary robot state machine
// if the robot is off, this jusrt returns
//======================================================================

void robot_run();
void robot_run_start();
void robot_run_stop();

#endif // _ROBOT_RUN_H_

