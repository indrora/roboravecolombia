//======================================================================
// prototype sketch - DIST_SENSOR example
//
// all robot actions will go here
//======================================================================


#include "parser.h"
#include "dist_sensor.h"
#include "memory_free.h"


//======================================================================
// this is the table of user commands
// When the command is typed into the "send" box in the Serial Monitor,
// the table is searched for the command.
// When the command is found, the function in the PARSER_CALLBACK( function )
// is called. That function can then use the arguments for the action
//======================================================================

Command user_commands[] =
{
    //=============================================================
    // First item is the command name
    // second item is the number of arguments it takes.
    // third item is the name of the function to call.
    //=============================================================

    {"distread",    0,  PARSER_CALLBACK(cmd_dist_sensor_read),
              "distread - read the dist sensors"},

    {"distthresh",    0,  PARSER_CALLBACK(cmd_dist_sensor_thresh),
     "distthresh - set the distance threshold (no param resets the default)"},

    //=============================================================
    // This tells us we have hit the end of our command list.
    // Don't touch this. Don't put anything after this (it will be ignored!)
    //=============================================================

    {null,-1,null,null}
};

//======================================================================
// this structure is used by the distance sensor code
// you can use a function to change the threshhold
// dist_vals.dist == distance to the box (max value 1023)
// dist_vals.at_box == true if closer than the threshold
//======================================================================

DIST_SENSOR_VALS  dist_vals;


//======================================================================
// The Serial.begin(9600) dist must be in the setup() function
//======================================================================

void setup()
{
    Serial.begin(9600);

    //===================================================================
    // the distance sensor is the distance to the box
    //===================================================================

    dist_sensor_init( &dist_vals );
}


//======================================================================
// These are the command callback functions
// they are called by the parser for a specific command
//======================================================================

void cmd_dist_sensor_thresh( ARGS *arg_list )
{
    //==================================================================
    // no arguments: reset to the threshold - really re-do the init
    //==================================================================

    if( arg_list->num_arguments == 0 )
    {
        dist_sensor_init( &dist_vals );
        Serial.println( "reset the distance values" );
        return;
    }

    //==================================================================
    // check for one arg, and the value is less than the max
    //==================================================================

    if(  (arg_list->num_arguments != 1 )
      || (arg_list->values[ 0 ] > DIST_SENSOR_THRESH_MAX) )
    {
        Serial.print( "Need a threshold <= " );
        Serial.print( DIST_SENSOR_THRESH_MAX );
        Serial.println( "" );
        return;
    }

    //==================================================================
    // set the new thresh
    //==================================================================

    dist_sensor_thresh( &dist_vals, arg_list->values[ 0 ] );

    Serial.print( "dist threshold = " );
    Serial.print( dist_vals.thresh );
    Serial.println( "  " );
}

//======================================================================

void cmd_dist_sensor_read( ARGS *arg_list )
{
    //====================================================================
    // do the read and print out the values
    //====================================================================

    dist_sensor_read( &dist_vals );
    
    Serial.print( "dist = " );
    Serial.print( dist_vals.at_box );
#if 1
    Serial.print( "  (" );
    Serial.print( dist_vals.dist );
    Serial.print( ")" );
#endif

    Serial.println( "  " );
}


//======================================================================
// "run_parser();" must be in the loop() function
//======================================================================

void loop()
{
    if( Serial.available() )
        run_parser();
  
    memory_free_report();
}

