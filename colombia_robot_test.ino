//======================================================================
// prototype sketch
//
// all robot actions will go here
//======================================================================

#include <avr/pgmspace.h>
#include <EEPROM.h>

#include "parser.h"
#include "drive_motors.h"
#include "arm_motor.h"
#include "line_sensor.h"
#include "dist_sensor.h"



//======================================================================
// these are the user-settable values
// they are put into a structure/variable to make it easy to save and restore
// from EEPROM
//======================================================================

typedef struct
{
    //======================================================================
    // we need a type in the struct to determine if what
    // we read from EEPROM is valid, or we need to init it first
    // the value doesn't really matter - it just needs to be unsigned 16 bits
    //======================================================================

#define TYPE_USER_SETTINGS  0xBEEF

    unsigned int        type;

    //======================================================================
    // this is the user-settable speed
    // see drive_motor_speed()
    // we initialize it to the max speed
    //  byte drive_speed = MOTOR_MAX_SPEED;
    //======================================================================

    byte                drive_speed;

    //======================================================================
    // these are the user units
    // this is the base number of milliseconds a user command executes
    // ie "fwd 1" is this number of units * 1
    // this value is in number of milliseconds and defaults to 100 mSec
    // this is also the slowest we want to go
    // NOTE: the command arguments are ints,
    // so the largest value allowed is 32000
    //======================================================================

#define USER_UNITS_DEFAULT      100         // smallest amount of move time

#define USER_UNITS_MIN          (USER_UNITS_DEFAULT / 2)
#define USER_UNITS_MAX          2000

    unsigned long       drive_units;

    //======================================================================
    // this is the user-settable speed
    // see arm_motor_speed()
    // we initialize it to the max speed
    //  byte arm_drive_speed = ARM_MOTOR_MAX_SPEED;
    //======================================================================

    byte                arm_drive_speed;

    //======================================================================
    // this is used for the distance sensor
    // the main thing we save is the threshold
    //======================================================================

    unsigned int        dist_thresh;

} USER_SETTINGS_t;

//======================================================================
// these are the global values needed for the various commands
// user_settings are saved to EEPROM when changed and restored on boot
// Te only thing we need to save for dist_vals is the threshold
//======================================================================

USER_SETTINGS_t user_settings;

DIST_SENSOR_VALS    dist_vals;

//======================================================================
// this is the table of user commands
// When the command is typed into the "send" box in the Serial Monitor,
// the table is searched for the command.
// When the command is found, the function in the PARSER_CALLBACK( function )
// is called. That function can then use the arguments for the action
//
// First item is the command name
// second item is the number of arguments it takes.
// third item is the name of the function to call.
//======================================================================

Command user_commands[] =
{
    //=============================================================
    // these are the driver motor commands
    //=============================================================

    {"fwd",   1, PARSER_CALLBACK(cmd_fwd),   "fwd <#units>"},
    {"back",  1, PARSER_CALLBACK(cmd_back),  "back <#units>"},
    {"left",  1, PARSER_CALLBACK(cmd_left),  "left <#units>"},
    {"right", 1, PARSER_CALLBACK(cmd_right), "right <#units>"},

    {"spin_left",  1,  PARSER_CALLBACK(cmd_spin_left), "spin_left <#units>"},
    {"spin_right", 1,  PARSER_CALLBACK(cmd_spin_right), "spin_right <#units>"},

    {"line_left",  3,  PARSER_CALLBACK(cmd_line_left),
                        "line_left <#fwd> <#spin> <#right>" },
    {"line_right", 3,  PARSER_CALLBACK(cmd_line_right),
                        "line_right <#fwd> <#spin> <#right>" },
    {"line_follow", 2,  PARSER_CALLBACK(cmd_follow_line),
                        "line_follow <#fwd> <#spin> <#right>" },

    {"drive_speed",  0,  PARSER_CALLBACK(cmd_drive_speed),
               "drive_speed [<speed>] (50..255)"},

    {"drive_units",  0,  PARSER_CALLBACK(cmd_drive_units),
               "drive_units [<units>] (100..3000)"},

    //=============================================================
    // these are for the ARM motor
    //=============================================================

    {"arm_fwd",    1,  PARSER_CALLBACK(cmd_arm_fwd), "armfwd <#milliSec>"},
    {"arm_back",   1,  PARSER_CALLBACK(cmd_arm_back), "armback <milliSec>"},

    {"arm_speed",  0,  PARSER_CALLBACK(cmd_arm_speed),
               "armspeed [<speed>] (50..255)"},

    //=============================================================
    // these commands are for the line sensor
    //=============================================================

    {"line_read",    0,  PARSER_CALLBACK(cmd_line_sensor_read),
              "read the line sensors"},

    //=============================================================
    // these are for the distance sensor
    //=============================================================

    {"dist_read",    0,  PARSER_CALLBACK(cmd_dist_sensor_read),
              "read the dist sensors"},

    {"dist_thresh",    0,  PARSER_CALLBACK(cmd_dist_sensor_thresh),
                    "set dist thresh"},

    //=============================================================
    // print the parameters
    // any argument will reset the params
    //=============================================================

    {"params",    0,  PARSER_CALLBACK(cmd_print_params),
                            "params [<1:reset>]"},

    //=============================================================
    // This tells us we have hit the end of our command list.
    // Don't touch this. Don't put anything after this (it will be ignored!)
    //=============================================================

    {null,-1,null,null}
};


//======================================================================
// The Serial.begin(9600) line must be in the setup() function
//======================================================================

void setup()
{
    //===================================================================
    // the serial port is used for the commands thru the Arduino serial monitor
    //===================================================================

    Serial.begin(9600);

    Serial.println( "Starting the serial command line" );

    //===================================================================
    // get the values from EEPROM
    // we read into user_settings and check the type
    // if the type is bad, we fill the struct and write it out
    //
    // these functions are at the end of this file
    //===================================================================

    eeprom_read_user_settings();
    if( user_settings.type != TYPE_USER_SETTINGS )
    {
        eeprom_fill_user_settings();
        eeprom_write_user_settings();
    }

    //===================================================================
    // the drive motors make the robot move forward, back, left, right
    //===================================================================

    drive_motors_init();

    //===================================================================
    // The ARM motor is for the ping-pong ball magazine
    //===================================================================

    arm_motor_init();

    //===================================================================
    // the line sensors are for the line-follower
    //===================================================================

    line_sensor_init();

    //===================================================================
    // the distance sensor is the distance to the box
    //===================================================================

    dist_sensor_init( &dist_vals );
    dist_vals.thresh = user_settings.dist_thresh;

}

//======================================================================
// this functio fills the user_settings areuct with the default values
//======================================================================

void eeprom_fill_user_settings()
{
    user_settings.type           = TYPE_USER_SETTINGS;
    user_settings.dist_thresh    = DIST_SENSOR_THRESH;
    user_settings.drive_speed     = MOTOR_MAX_SPEED;
    user_settings.drive_units     = USER_UNITS_DEFAULT;
    user_settings.arm_drive_speed = ARM_MOTOR_MAX_SPEED;
}

//======================================================================
//    {"params",    0,  PARSER_CALLBACK(cmd_print_params), "print params"},
//
// any argument will reset the params
//======================================================================

void cmd_print_params( ARGS *arg_list )
{
    if( ARG_COUNT == 1 )
    {
        eeprom_fill_user_settings();
        eeprom_write_user_settings();
    }

    Serial.print(   "dist_thresh     = " );
    Serial.println(  user_settings.dist_thresh );
    Serial.print(   "drive_speed     = " );
    Serial.println(  user_settings.drive_speed );
    Serial.print(   "drive_units     = " );
    Serial.println(  user_settings.drive_units );
    Serial.print(   "arm_drive_speed = " );
    Serial.println(  user_settings.arm_drive_speed );
}

//======================================================================
// These are the callback functions for the drive motors
// they are called by the parser for a specific command
//======================================================================
//
// cmd_drive_speed()
//
// set the speed from 0 to 255.
// 0 is full stop (no motion at all for any command) and 255 is full speed
// we will make the minimum speed that can be set at 50 - really slow,
// but the robot will move
//======================================================================

void cmd_drive_speed( ARGS *arg_list )
{
    //====================================================================
    // if 0 arguments, then print out the drivespeed
    //====================================================================

    if( ARG_COUNT == 0 )
    {
        Serial.print( "drivespeed = " );
        Serial.println( user_settings.drive_speed );
        return;
    }

    //====================================================================
    // if an argument was given, it must be one number between
    // MOTOR_MIN_USER_SPEED and MOTOR_MAX_SPEED
    //====================================================================

    if(  (ARG_COUNT != 1)
      || (ARG_0 < MOTOR_MIN_USER_SPEED)
      || (ARG_0 > MOTOR_MAX_SPEED) )
    {
        Serial.print( "drivespeed must be a speed " );
        Serial.print( MOTOR_MIN_USER_SPEED );
        Serial.print( " to " );
        Serial.println( MOTOR_MAX_SPEED );
        return;
    }

    //====================================================================
    // set the new value and write to EEPROM
    //====================================================================

    user_settings.drive_speed = ARG_0;
    eeprom_write_user_settings();

    Serial.print( "drivespeed = " );
    Serial.println( user_settings.drive_speed );
}



//======================================================================
//
// cmd_drive_units()
//
// set the speed from 0 to 255.
// 0 is full stop (no motion at all for any command) and 255 is full speed
// we will make the minimum speed that can be set at 50 - really slow,
// but the robot will move
//======================================================================

void cmd_drive_units( ARGS *arg_list )
{
    //====================================================================
    // if 0 arguments, then print out the drivespeed
    //====================================================================

    if( ARG_COUNT == 0 )
    {
        Serial.print( "units = " );
        Serial.println( user_settings.drive_units );
        return;
    }

    //====================================================================
    // if an argument was given, it must be one number between
    // MOTOR_MIN_USER_SPEED and MOTOR_MAX_SPEED
    //====================================================================

    if(  (ARG_COUNT != 1)
      || (ARG_0 < USER_UNITS_MIN)
      || (ARG_0 > USER_UNITS_MAX) )
    {
        Serial.print( "units must be " );
        Serial.print( USER_UNITS_MIN );
        Serial.print( " to " );
        Serial.println( USER_UNITS_MAX );
        return;
    }

    //====================================================================
    // set the new value and write to EEPROM
    //====================================================================

    user_settings.drive_units = (unsigned long) ARG_0;
    eeprom_write_user_settings();

    Serial.print( "driveunits = " );
    Serial.println( user_settings.drive_units );
}


//======================================================================
// move the robot forward
//======================================================================

void cmd_fwd( ARGS *arg_list )
{
    if( ARG_COUNT != 1 )
    {
        Serial.println( "need number of units" );
        return;
    }

    //====================================================================
    // start the motion
    // delay values[0] second (user_settings.drive_units milliSec)
    // stop motor
    //====================================================================

    drive_motors_fwd( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_0 );   // delay values[0] units
    drive_motors_stop();
}


//======================================================================
// move the robot backwards
//======================================================================

void cmd_back( ARGS *arg_list )
{
    if( ARG_COUNT != 1 )
    {
        Serial.println( "need number of units" );
        return;
    }

    //====================================================================
    // start the motion
    // delay values[0] second (user_settings.drive_units milliSec)
    // stop motor
    //====================================================================

    drive_motors_back( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_0 );   // delay values[0] units
    drive_motors_stop();
}


//======================================================================
// move the robot to the left
//======================================================================

void cmd_left( ARGS *arg_list )
{
    if( ARG_COUNT != 1 )
    {
        Serial.println( "need number of units" );
        return;
    }

    //====================================================================
    // start the motion
    // delay values[0] second (user_settings.drive_units milliSec)
    // stop motor
    //====================================================================

    drive_motors_left( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_0 );   // delay values[0] units
    drive_motors_stop();
}


//======================================================================
// move the robot to the right
//======================================================================

void cmd_right( ARGS *arg_list )
{
    if( ARG_COUNT != 1 )
    {
        Serial.println( "need number of units" );
        return;
    }

    //====================================================================
    // start the motion
    // delay values[0] second (user_settings.drive_units milliSec)
    // stop motor
    //====================================================================

    drive_motors_right( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_0 );   // delay values[0] units
    drive_motors_stop();
}


//======================================================================
// spin the robot to the left
//======================================================================

void cmd_spin_left( ARGS *arg_list )
{
    if( ARG_COUNT != 1 )
    {
        Serial.println( "need number of units" );
        return;
    }

    //====================================================================
    // start the motion
    // delay values[0] second (user_settings.drive_units milliSec)
    // stop motor
    //====================================================================

    drive_motors_spin_left( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_0 );   // delay values[0] units
    drive_motors_stop();
}


//======================================================================
// spin the robot to the right
//======================================================================

void cmd_spin_right( ARGS *arg_list )
{
    if( ARG_COUNT != 1 )
    {
        Serial.println( "need number of units" );
        return;
    }

    //====================================================================
    // start the motion
    // delay values[0] second (user_settings.drive_units milliSec)
    // stop motor
    //====================================================================
    
    drive_motors_spin_right( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_0 );   // delay values[0] units
    drive_motors_stop();
}


//======================================================================
// function to find the line with the left sensor
// function to find the line with the right sensor
//
// returns when the sensor finds the line
//
// arguments:
//      *line_vals          pointer to a LINE_SENSOR_VALS variable
//      drive_units         the drive_units to delay when spinning
//                          to the right/left when looking for the line
//
// on return, *line_vals variable is filled with the latest line sensor
// readings. This is useful to determine if you have hit a T or sideways T
//
// The test is
//
// loop
//    read sensor
//    if( not at line )
//       spin right/left at speed units
//======================================================================

void find_line_to_left( LINE_SENSOR_VALS *line_vals, int num_units)
{
    while( 1 )
    {
        line_sensor_read( line_vals );
    
#if 1
        Serial.print( "units = " );
        Serial.print( num_units );
#endif
        Serial.print( "; left = " );
        Serial.print( line_vals->left_sensor );
        Serial.println( "" );

        //===============================================================
        // if we have found the line, we want to break out of the loop
        //===============================================================

        if( line_vals->left_sensor )
        {
            Serial.println( "found" );
            break;
        }
        else
        {
            drive_motors_spin_right( user_settings.drive_speed );
            delay( user_settings.drive_units * num_units );
            drive_motors_stop();
        }

        //===============================================================
        // because we are in a loop, and maybe in a "rep" repeated command,
        // we need to check if the user has stopped the repeated command
        // stop_repeat() will return "true" if the repeaded command is stopped
        //===============================================================

        if( stop_repeat() )
        {
            Serial.println( "stop_repeat" );
            break;
        }
    }
}


//======================================================================
// see the comments in find_line_left_color()
//======================================================================

void find_line_to_right( LINE_SENSOR_VALS *line_vals, int num_units )
{
    while( 1 )
    {
        line_sensor_read( line_vals );
    
#if 1
        Serial.print( "units = " );
        Serial.print( num_units );
#endif

        Serial.print( "; right = " );
        Serial.print( line_vals->right_sensor );
        Serial.println( "" );

        if( line_vals->right_sensor )
        {
            Serial.println( "found" );
            break;
        }
        else
        {
            drive_motors_spin_left( user_settings.drive_speed );
            delay( user_settings.drive_units * num_units );
            drive_motors_stop();
        }

        if( stop_repeat() )
        {
            Serial.println( "stop_repeat" );
            break;
        }
    }
}




//======================================================================
// function to move away from the line with the left sensor
// function to move away from the line with the right sensor
//
// returns when the sensor does not the line
// meaning we start on the line, and we wantto move away from it
//
// arguments:
//      *line_vals          pointer to a LINE_SENSOR_VALS variable
//      drive_units         the drive_units to delay when spinning
//                          to the right/left when looking for the line
//
// on return, *line_vals variable is filled with the latest line sensor
// readings. This is useful to determine if you have hit a T or sideways T
//
// The test is
//
// loop
//    read sensor
//    if( not at line )
//       spin right/left at speed units
//======================================================================

void move_away_from_line_left( LINE_SENSOR_VALS *line_vals, int num_units)
{
    while( 1 )
    {
        line_sensor_read( line_vals );
    
#if 1
        Serial.print( "units = " );
        Serial.print( num_units );
#endif
        Serial.print( "; left = " );
        Serial.print( line_vals->left_sensor );
        Serial.println( "" );

        //===============================================================
        // we could find a TEE
        //===============================================================

        if( line_vals->right_sensor && line_vals->left_sensor )
        {
            Serial.println( "found TEE" );
            break;
        }

        //===============================================================
        // if we have are not on the line, we want to break out of the loop
        //===============================================================

        if( line_vals->left_sensor == false )
        {
            Serial.println( "found" );
            break;
        }

        //===============================================================
        // we need to move away from the line
        //===============================================================

        else
        {
            drive_motors_spin_left( user_settings.drive_speed );
            delay( user_settings.drive_units * num_units );
            drive_motors_stop();
        }

        //===============================================================
        // because we are in a loop, and maybe in a "rep" repeated command,
        // we need to check if the user has stopped the repeated command
        // stop_repeat() will return "true" if the repeaded command is stopped
        //===============================================================

        if( stop_repeat() )
        {
            Serial.println( "stop_repeat" );
            break;
        }
    }
}


//======================================================================
// see the comments in find_line_left_color()
//======================================================================

void move_away_from_line_right( LINE_SENSOR_VALS *line_vals, int num_units)
{
    while( 1 )
    {
        line_sensor_read( line_vals );
    
#if 1
        Serial.print( "units = " );
        Serial.print( num_units );
#endif

        Serial.print( "; right = " );
        Serial.print( line_vals->right_sensor );
        Serial.println( "" );

        if( line_vals->right_sensor && line_vals->left_sensor )
        {
            Serial.println( "found TEE" );
            break;
        }

        if( line_vals->right_sensor == false )
        {
            Serial.println( "found" );
            break;
        }
        else
        {
            drive_motors_spin_right( user_settings.drive_speed );
            delay( user_settings.drive_units * num_units );
            drive_motors_stop();
        }

        if( stop_repeat() )
        {
            Serial.println( "stop_repeat" );
            break;
        }
    }
}



//======================================================================
// follow the left side of the line
//
// line_left <#units_fwd> <#units_left> <#units_rt>
//======================================================================

void cmd_line_left( ARGS *arg_list )
{
    //====================================================================
    // the struct to get the line sensor values with
    //====================================================================

    LINE_SENSOR_VALS  line_vals;

    //====================================================================
    // need three parameters
    //====================================================================

    if( ARG_COUNT != 3 )
    {
        Serial.println( "need 3 args" );
        return;
    }

#if 0
    Serial.print( "args: " );
    Serial.print( ARG_0 );
    Serial.print( "; " );
    Serial.print( ARG_1 );
    Serial.print( "; " );
    Serial.print( ARG_2 );
    Serial.println( "" );
#endif

    //====================================================================
    // assumes not on the line
    // go forward X units
    // loop
    //    read sensor
    //    if( not at line )
    //       turn left Y units
    // turn right Z units
    //====================================================================
    // go forward X units
    //====================================================================

    drive_motors_fwd( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_0 );   // delay values[0] units
    drive_motors_stop();

    //====================================================================
    // now find the line to the left
    //
    // after the call, line_vals.left_sensor can be used to determine
    // if you hit a sideways T
    //====================================================================

    find_line_to_left( &line_vals, ARG_1 );

    //====================================================================
    // turn right Z units to straighten out
    //====================================================================

    drive_motors_spin_left( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_2 );
    drive_motors_stop();
}


//======================================================================
// follow the right side of the line
//======================================================================

void cmd_line_right( ARGS *arg_list )
{
    //====================================================================
    // the struct to get the line sensor values with
    //====================================================================

    LINE_SENSOR_VALS  line_vals;

    //====================================================================
    // need three parameters
    //====================================================================

    if( ARG_COUNT != 3 )
    {
        Serial.println( "need 3 args" );
        return;
    }

#if 0
    Serial.print( "args: " );
    Serial.print( ARG_0 );
    Serial.print( "; " );
    Serial.print( ARG_1 );
    Serial.print( "; " );
    Serial.print( ARG_2 );
    Serial.println( "" );
#endif

    //====================================================================
    // assumes not on the line
    // go forward X units
    // loop
    //    read sensor
    //    if( not at line )
    //       turn right Y units
    // turn right Z units
    //====================================================================
    // go forward X units
    //====================================================================

    drive_motors_fwd( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_0 );   // delay values[0] units
    drive_motors_stop();

    //====================================================================
    // now find the line to the right
    //
    // after the call, line_vals.left_sensor can be used to determine
    // if you hit a sideways T
    //====================================================================

    find_line_to_right( &line_vals, ARG_1);

    //====================================================================
    // turn right Z units to straighten out
    //====================================================================

    drive_motors_spin_right( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_2 );
    drive_motors_stop();
}




//======================================================================
// follow the line
//======================================================================

void cmd_follow_line( ARGS *arg_list )
{
    //====================================================================
    // the struct to get the line sensor values with
    //====================================================================

    LINE_SENSOR_VALS  line_vals;

    //====================================================================
    // need two parameters
    //====================================================================

    if( ARG_COUNT != 2 )
    {
        Serial.println( "need 2 args" );
        return;
    }

#if 0
    Serial.print( "args: " );
    Serial.print( ARG_0 );
    Serial.print( "; " );
    Serial.print( ARG_1 );
    Serial.println( "" );
#endif

    //====================================================================
    // assumes not on the line
    // go forward X units
    // loop
    //    read sensor
    //    if( not at line )
    //       turn right Y units
    // turn right Z units
    //====================================================================
    // go forward X units
    //====================================================================

    drive_motors_fwd( user_settings.drive_speed );
    delay( user_settings.drive_units * ARG_0 );   // delay values[0] units
    drive_motors_stop();

    //====================================================================
    // now look at the line sensors.
    // if we see the line with the left, spin right Y (ARG_1) units to white
    // if we see the line with the right, spin left Y (ARG_1) units to white
    //
    // There are two special cases: a T and a sideways T
    // after the call, line_vals.left_sensor can be used to determine
    // if you hit a sideways T
    // We are going to ignore those cases for now
    //====================================================================

    line_sensor_read( &line_vals );

    if( ( !line_vals.left_sensor) && ( !line_vals.right_sensor) )
    {
        //================================================================
        // still centered between lines, no need to spin
        //================================================================

        Serial.println( "straight" );
    }
    else if( line_vals.left_sensor && line_vals.right_sensor )
    {
        //================================================================
        // special case, hit a T or are on the line and a sideways T
        // ignore turning for now
        //================================================================

        Serial.println( "TEE" );
    }
    else if( line_vals.left_sensor )
    {
        move_away_from_line_left( &line_vals, ARG_1 );
    }
    else if( line_vals.right_sensor )
    {
        move_away_from_line_right( &line_vals, ARG_1 );
    }

    //====================================================================
    // see if we hit the tee
    // we do this after the call also, because the left or right move
    // could detect the TEE or a sideways TEE
    //====================================================================

    if( line_vals.left_sensor && line_vals.right_sensor )
    {
        //================================================================
        // special case, hit a T or are on the line and a sideways T
        // ignore turning for now
        //================================================================

        Serial.println( "TEE" );
    }
}




//======================================================================
// These are the callback functions for the ARM motor
// they are called by the parser for a specific command
//======================================================================
//
// cmd_arm_speed()
//
// set the speed from 0 to 255.
// 0 is full stop (no motion at all for any command) and 255 is full speed
// we will make the minimum speed that can be set at 50 - really slow,
// but the robot will move
//======================================================================

void cmd_arm_speed( ARGS *arg_list )
{
    //====================================================================
    // if 0 arguments, then print out the armspeed
    //====================================================================

    if( ARG_COUNT == 0 )
    {
        Serial.print( "armspeed = " );
        Serial.println( user_settings.arm_drive_speed );
        return;
    }

    //====================================================================
    // if an argument was given, it must be one number between
    // ARM_MOTOR_MIN_USER_SPEED and ARM_MOTOR_MAX_SPEED
    //====================================================================

    if(  (ARG_COUNT != 1)
      || (ARG_0 < ARM_MOTOR_MIN_USER_SPEED)
      || (ARG_0 > ARM_MOTOR_MAX_SPEED) )
    {
        Serial.print( "armspeed must be a speed " );
        Serial.print( ARM_MOTOR_MIN_USER_SPEED );
        Serial.print( " to " );
        Serial.println( ARM_MOTOR_MAX_SPEED );
        return;
    }

    //====================================================================
    // set the arm_drive_speed to the command argument value
    //====================================================================

    user_settings.arm_drive_speed = ARG_0;
    eeprom_write_user_settings();

    Serial.print( "armspeed = " );
    Serial.println( user_settings.arm_drive_speed );
}


//======================================================================
// argument is number milliSec
//======================================================================

void cmd_arm_fwd( ARGS *arg_list )
{
    if( ARG_COUNT != 1 )
    {
        Serial.println( "needs number of milliSec" );
        return;
    }

    //====================================================================
    // start the motion, delay values[0] milliSec, stop motor
    //====================================================================

    arm_motor_fwd( user_settings.arm_drive_speed );
    delay( ARG_0 );   // delay values[0] milliSec
    arm_motor_stop();
}


//======================================================================
// argument is number milliSec
//======================================================================

void cmd_arm_back( ARGS *arg_list )
{
    if( ARG_COUNT != 1 )
    {
        Serial.println( "needs number of milliSec" );
        return;
    }

    //====================================================================
    // start the motion, delay values[0] milliSec, stop motor
    //====================================================================

    arm_motor_back( user_settings.arm_drive_speed );
    delay( ARG_0 );   // delay values[0] milliSec
    arm_motor_stop();
}



//======================================================================
// These are the callback functions for the line sensor
//======================================================================

void cmd_line_sensor_read( ARGS *arg_list )
{
    //====================================================================
    // this is where the values are put
    // the variable is used only when this function is called,
    // so we can define it here (locally) instead of globally
    //====================================================================

    LINE_SENSOR_VALS  line_vals;

    //====================================================================
    // do the read and print out the values
    //====================================================================

    line_sensor_read( &line_vals );
    
    Serial.print( "left = " );
    Serial.print( line_vals.left_sensor );

    Serial.print( "; right = " );
    Serial.print( line_vals.right_sensor );

    Serial.println( "  " );
}



//======================================================================
// These are the callback functions for the distance sensor
// they are called by the parser for a specific command
//======================================================================

void cmd_dist_sensor_thresh( ARGS *arg_list )
{
    //==================================================================
    // no arguments: reset to the threshold - really re-do the init
    //==================================================================

    if( ARG_COUNT == 0 )
    {
        dist_sensor_init( &dist_vals );
        eeprom_write_user_settings();

        Serial.print( "dist threshold = " );
        Serial.print( dist_vals.thresh );
        Serial.println( "  " );
        return;
    }

    //==================================================================
    // check for one arg, and the value is less than the max
    //==================================================================

    if(  (ARG_COUNT != 1 )
      || (ARG_0 > DIST_SENSOR_THRESH_MAX) )
    {
        Serial.print( "Need a threshold <= " );
        Serial.print( DIST_SENSOR_THRESH_MAX );
        Serial.println( "" );
        return;
    }

    //==================================================================
    // set the new thresh
    //==================================================================

    dist_sensor_thresh( &dist_vals, ARG_0 );
    eeprom_write_user_settings();

    Serial.print( "dist threshold = " );
    Serial.print( dist_vals.thresh );
    Serial.println( "  " );
}

//======================================================================
// read the sensor
//======================================================================

void cmd_dist_sensor_read( ARGS *arg_list )
{
    //====================================================================
    // do the read and print out the values
    //====================================================================

    dist_sensor_read( &dist_vals );

    Serial.print( "dist = " );
    Serial.print( dist_vals.at_box );
#if 1
    Serial.print( "  (" );
    Serial.print( dist_vals.dist );
    Serial.print( ")" );
#endif

    Serial.println( "  " );
}



//======================================================================
// "run_parser();" must be in the loop() function
//======================================================================

void loop()
{
    //==================================================================
    // detect a line from the Arduino serial monitor
    // (NOTE: Must have the "line feed" selected in the lower-right corner
    // of the window, and type in the single bar at the top of the window)
    //==================================================================

    if( Serial.available() )
        run_parser();
}


//======================================================================
// the eeprom functions
// we use the EEPROM library
// The trick here is we create a byte pointer to the start of user_settings
// and treat it like a byte array, length = sizeof(user_settings)
//======================================================================

void eeprom_read_user_settings()
{
    byte *ptr;
    int  i;

    ptr = (byte *) &user_settings;
    
    for( i = 0; i < sizeof(user_settings); i++ )
    {
        ptr[ i ] = EEPROM.read( i );
    }
}

//======================================================================
// write the current values in user_settings to the EEPROM
//======================================================================

void eeprom_write_user_settings()
{
    byte *ptr;
    int  i;

    ptr = (byte *) &user_settings;
    
    for( i = 0; i < sizeof(user_settings); i++ )
    {
        EEPROM.write( i, ptr[ i ] );
    }
}

//======================================================================
//======================================================================


