//======================================================================
// robot_run.cpp
//
// This is the code, state machine, and other stuff for the running
// of the robot
//
// start and stop the robot actions of following the line
//======================================================================

#include <Arduino.h>

#include "robot_run.h"

#include "drive_motors.h"
#include "arm_motor.h"
#include "line_sensor.h"
#include "dist_sensor.h"
#include "user_settings.h"

//======================================================================
// general flag for the robot on or off
//======================================================================

byte robot_running = 0;

//======================================================================
// the general robot states
// notice this assumes the robot is in the starting state
// at the beginning of the track
//======================================================================

#define ROBOT_STATE_STOP        0   // follow the line to the T
#define ROBOT_STATE_START       1   // follow the line to the T
#define ROBOT_STATE_TEE         2   // turn right at the T
#define ROBOT_STATE_BOX         3   // stop at the box
#define ROBOT_STATE_DUMP        4   // dump the ping-pong
#define ROBOT_STATE_BOX         5        // stop at the box
#define ROBOT_STATE_DUMP        6        // dump the ping-pong balls
#define ROBOT_STATE_TURN_180    7        // turn around
#define ROBOT_STATE_FIND_LEFT   8        // find the inverse T
#define ROBOT_STATE_TURN_LEFT   9        // turn left
#define ROBOT_STATE_GO_HOME     10       // go home


byte robot_state = ROBOT_STATE_STOP;

//======================================================================
// these are the return values for the various movement functions
//======================================================================

#define FOLLOW_LINE             0   // nominal
#define FOUND_TEE               1   // found the TEE
#define AFTER_TEE               2   // after turn at the TEE
#define FOUND_BOX               3
#define DUMP_DONE               4
#define TURN_180_DONE           5
#define TURN_FOUND_LEFT         6
#define TURN_AFTER_LEFT         7
#define FOUND_HOME              8

//======================================================================
// this controls how much the drive motors, etc are on
//======================================================================

#define MOTOR_MOVE_TIME_MS      50  // move the motor for this amount

//======================================================================
// forward function declarations
//======================================================================

static byte robot_find_tee();
static byte robot_turn_right_at_tee();
static byte robot_find_box();
static byte robot_do_dump();
static byte robot_turn_180();
static byte robot_find_left();
static byte robot_turn_left();
static byte robot_go_home();

static byte follow_line( byte end_status );

//======================================================================
// the top level start/stop functions
// these are called by the CLI
//======================================================================

void robot_run_start()
{
    Serial.println( "Start the robot" );
    robot_running = 1;
    robot_state = ROBOT_STATE_START;
}


void robot_run_stop()
{
    Serial.println( "Stop the robot" );
    robot_running = 0;
    robot_state   = ROBOT_STATE_STOP;
}


//======================================================================
// this is the primary robot state machine
// if the robot is off, this jusrt returns
//======================================================================

void robot_run()
{
    byte    ret;

    //==================================================================
    // if robot off, just return
    //==================================================================

    if( robot_running == 0 )
        return;

    //==================================================================
    // this is the robot following the line
    //==================================================================

    switch( robot_state )
    {
    case ROBOT_STATE_STOP:       // stop the robot
        robot_running = 0;
        break;

    case ROBOT_STATE_START:      // follow the line to the T

        Serial.println( "START: Follow line" );
        ret = robot_find_tee();
        if( ret == FOUND_TEE )
            robot_state = ROBOT_STATE_TEE;
        break;

    case ROBOT_STATE_TEE:        // turn right at the T

        Serial.println( "TEE: turn right" );
        ret = robot_turn_right_at_tee();
        if( ret == AFTER_TEE )
            robot_state = ROBOT_STATE_BOX;
        break;

    case ROBOT_STATE_BOX:        // stop at the box
        Serial.println( "BOX: " );
        ret = robot_find_box();
        if( ret == FOUND_BOX )
            robot_state = ROBOT_STATE_DUMP;
        break;

    case ROBOT_STATE_DUMP:        // dump the ping-pong balls
        Serial.println( "DUMP: " );
        ret = robot_do_dump();
        if( ret == DUMP_DONE )
            robot_state = ROBOT_STATE_TURN_180;
        break;

    case ROBOT_STATE_TURN_180:        // turn around
        Serial.println( "TURN_180: " );
        ret = robot_turn_180();
        if( ret == TURN_180_DONE )
            robot_state = ROBOT_STATE_FIND_LEFT;
        break;

    case ROBOT_STATE_FIND_LEFT:        // find the inverse T
        Serial.println( "FIND_LEFT: " );
        ret = robot_find_left();
        if( ret == TURN_FOUND_LEFT )
            robot_state = ROBOT_STATE_TURN_LEFT;
        break;

    case ROBOT_STATE_TURN_LEFT:        // turn left
        Serial.println( "TURN_LEFT: " );
        ret = robot_turn_left();
        if( ret == TURN_AFTER_LEFT )
            robot_state = ROBOT_STATE_GO_HOME;
        break;

    case ROBOT_STATE_GO_HOME:        // go home
        Serial.println( "GO_HOME: " );
        ret = robot_go_home();
        if( ret == FOUND_HOME )
            robot_state = ROBOT_STATE_STOP;
        break;


    default:
        Serial.print( "bad robot state " );
        Serial.println( robot_state );
        break;
    }
}

//======================================================================
// the robot actions
//======================================================================
// follow a straight line
//======================================================================

static LINE_SENSOR_VALS    line_vals;

static void rr_line_read( LINE_SENSOR_VALS *val )
{
    //==================================================================
    // look at the line sensors
    // if left detected, turn left && visa-versa
    // if no line found, move forward
    // if both found, found the tee
    //==================================================================

    line_sensor_read( val );

#if 1
    Serial.print( "left = " );
    Serial.print( val->left_sensor );
    Serial.print( "; right = " );
    Serial.print( val->right_sensor );
    Serial.println( "  " );
#endif

}

//======================================================================
// follow a line
// return the given status if both sensors are true
// otherwise return FOLLOW_LINE
// 
//
// look at the line sensors
// if left detected, turn left && visa-versa
// if no line found, move forward
// if both found, found the tee
//
// uses the global line_vals
// assumes the usr has filled it in
//======================================================================

static byte follow_line( byte end_status )
{
    byte                line_mask;
    byte                ret;

    //==================================================================
    // we are going to make a mask for the four cases
    // that way we can use a switch statement
    //==================================================================

    line_mask = line_vals.left_sensor | (line_vals.right_sensor << 1);
    Serial.print( "line_mask " );
    Serial.println( line_mask, HEX );

    ret = FOLLOW_LINE;

    switch( line_mask )
    {
    case 0x00:                  // no line detected
        drive_motors_fwd( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_stop();
        break;

    case 0x01:                  // left sensor
        drive_motors_spin_left( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_fwd( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_stop();
        break;

    case 0x02:                  // right sensor
        drive_motors_spin_right( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_fwd( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_stop();
        break;

    case 0x03:                  // TEE found
        ret = end_status;
        break;
    }

    return( ret );
}


//======================================================================
// follow a line
//======================================================================

static byte robot_find_tee()
{
    byte ret;

    //==================================================================
    // get the line sensor readings, then follow the line until T
    //==================================================================

    rr_line_read( &line_vals );
    ret = follow_line( FOUND_TEE );

    return( ret );
}

//======================================================================
// we turn right at the T
// need to go forward and start spinning until right sensor sees line
// we will do the entire action for the single call
// return AFTER_TEE when done
//======================================================================

static byte robot_turn_right_at_tee()
{
    //==================================================================
    // go forward half way
    //==================================================================

    drive_motors_fwd( user_settings.user_speed );
    delay( MOTOR_MOVE_TIME_MS * 15 );
    drive_motors_stop();

    //==================================================================
    // now start spinning until see the right sensor go high
    //==================================================================

    do
    {
        drive_motors_spin_right( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_stop();

        rr_line_read( &line_vals );
    }
    while( line_vals.right_sensor == 0 );

    //==================================================================
    // now start spinning until see the right sensor go low
    //==================================================================

    do
    {
        drive_motors_spin_right( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_stop();

        rr_line_read( &line_vals );
    }
    while( line_vals.right_sensor == 1 );

    return( AFTER_TEE );
}

//=====================================================================
// find the box. use the distance sensor
//if( ret == FOUND_BOX )
//=====================================================================

static byte robot_find_box()
{
    //==================================================================
    // see what the distance is
    //==================================================================

    dist_sensor_read( &dist_vals );     // use the global with user threshold
    if( dist_vals.at_box == true )
        return( FOUND_BOX );

    //==================================================================
    // get the line sensor readings, then follow the line until T
    //==================================================================

    rr_line_read( &line_vals );
    follow_line( FOUND_TEE );       // ignore the return  no TEE

    return( FOLLOW_LINE );
}


//=====================================================================
// dump the hopper
//if( ret == DUMP_DONE )
//=====================================================================

static byte robot_do_dump()
{
    //==================================================================
    // move the arm forward, delay for the dump, then back
    //==================================================================

    arm_motor_fwd( user_settings.arm_user_speed );
    delay( DUMP_HOPPER_MOTOR_DELAY );   // delay values[0] milliSec
    arm_motor_stop();

    delay( 1000 * DUMP_HOPPER_WAIT_SEC );

    arm_motor_back( user_settings.arm_user_speed );
    delay( DUMP_HOPPER_MOTOR_DELAY );   // delay values[0] milliSec
    arm_motor_stop();

    return( DUMP_DONE );
}


//=====================================================================
// back the robot, spin 180
//
//if( ret == TURN_180_DONE )
//
//=====================================================================

static byte robot_turn_180()
{
    //==================================================================
    // go backwards to clear box
    //==================================================================

    drive_motors_back( user_settings.user_speed );
    delay( MOTOR_MOVE_TIME_MS * 10 );
    drive_motors_stop();

    //==================================================================
    // now start spinning until see the right sensor go high
    //==================================================================

    do
    {
        drive_motors_spin_right( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_stop();

        rr_line_read( &line_vals );
    }
    while( line_vals.right_sensor == 0 );

    //==================================================================
    // now start spinning until see the right sensor go low
    //==================================================================

    do
    {
        drive_motors_spin_right( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_stop();

        rr_line_read( &line_vals );
    }
    while( line_vals.right_sensor == 1 );

    return( TURN_180_DONE );
}


//=====================================================================
// move forward until left sensor finds line
// assumes runs in a straight line
//if( ret == TURN_FOUND_LEFT )
//=====================================================================

static byte robot_find_left()
{

    //==================================================================
    // go forward til the left sensor sees the line
    //==================================================================

    do
    {
        drive_motors_fwd( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_stop();

        rr_line_read( &line_vals );
    }
    while( line_vals.left_sensor == 0 );

    return( TURN_FOUND_LEFT );
}


//=====================================================================
//if( ret == TURN_AFTER_LEFT )
//=====================================================================

static byte robot_turn_left()
{
    //==================================================================
    // go backwards to clear box
    //==================================================================

    drive_motors_fwd( user_settings.user_speed );
    delay( MOTOR_MOVE_TIME_MS * 10 );
    drive_motors_stop();

    //==================================================================
    // now start spinning until see the right sensor go high
    //==================================================================

    do
    {
        drive_motors_spin_left( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_stop();

        rr_line_read( &line_vals );
    }
    while( line_vals.left_sensor == 0 );

    //==================================================================
    // now start spinning until see the right sensor go low
    //==================================================================

    do
    {
        drive_motors_spin_left( user_settings.user_speed );
        delay( MOTOR_MOVE_TIME_MS );
        drive_motors_stop();

        rr_line_read( &line_vals );
    }
    while( line_vals.left_sensor == 1 );

    return( TURN_AFTER_LEFT );
}


//=====================================================================
//if( ret == FOUND_HOME )
//=====================================================================

static byte robot_go_home()
{
    byte ret;

    //==================================================================
    // get the line sensor readings, then follow the line until T
    //==================================================================

    rr_line_read( &line_vals );
    ret = follow_line( FOUND_HOME );

    return( ret );
}




