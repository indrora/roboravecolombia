//======================================================================
// robot_pins.h
//
// these assignments are for the prototype robot made in Albuquerque
// Change these for the kit
// all are PWM outputs
//======================================================================

#ifndef _ROBOT_PINS_H_
#define _ROBOT_PINS_H_

//======================================================================
// These are for the drive motor
//
// all are PWM outputs
//======================================================================

#define LEFT_MOTOR_A        3
#define LEFT_MOTOR_B        5
#define LEFT_MOTOR_ENABLE   2

#define RIGHT_MOTOR_A       6
#define RIGHT_MOTOR_B       9
#define RIGHT_MOTOR_ENABLE  4

//======================================================================
// these are for the arm to dump the ping-pong balls
//======================================================================

#define ARM_MOTOR_A         10
#define ARM_MOTOR_B         11
#define ARM_MOTOR_ENABLE    7

//======================================================================
// sensor inputs
// these are analog inputs
//======================================================================

#define DIST_SENSOR         A0       // distance to box, close = higher

#define LINE_SENSOR_ENABLE  8        // enable the h-bridge channel (A,B)
#define LINE_LEFT_SENSOR    A1       // white = LOW, black = HIGH
#define LINE_RIGHT_SENSOR   A2       // white = LOW, black = HIGH

//======================================================================
//======================================================================


#endif // _ROBOT_PINS_H_

