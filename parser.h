//==============================================================
// parser include file.
// Dont worry if this looks ugly.
//==============================================================

#ifndef _PARSER_H_
#define _PARSER_H_
#include "parser_types.h"
#include "parser_support.h"

extern Command user_commands[];

void run_parser();
boolean stop_repeat();

#endif // _PARSER_H_

