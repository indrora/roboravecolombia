//======================================================================
// dist_sensor.cpp
//
// These are the functions needed for the ping-pong arm
//
// void dist_sensor_init()
// void dist_sensor_read()
//
// read the two line sensors - left and right
//======================================================================

#include <Arduino.h>
#include "robot_pins.h"
#include "dist_sensor.h"

//======================================================================
// init the user structure
//======================================================================

void dist_sensor_init( DIST_SENSOR_VALS *val )
{
    val->at_box = false;
    val->dist   = 0;
    val->thresh = DIST_SENSOR_THRESH;
}


//======================================================================
// set the threshold - a user value
//======================================================================

void dist_sensor_thresh( DIST_SENSOR_VALS *val, unsigned int thresh )
{
    val->thresh = thresh;
}

//======================================================================
// There are two GP2Y0D810Z0F sensors - one on the left and one on the right.
// Te trick is the sensor wants to return close to 0 for white
// and max for non-white.
// So, to remove jitter, we will take 4 readings and average them.
// If the reading average is below a threshold, it is white
// otherwise we will max for white.
// This is known as removing jitter.
//
// we use an unsigned int: 16 bits, 0..65535
// if we add 1023 * 64 (ie the max 64 times) we get 65472, or close to 65535
// We would not want to do 1023 * 65 = 66495 which would "roll over"
// to a small number (66495 - 65535 = 960)
// However, we are only going to take 4 numbers as the average
//======================================================================

void dist_sensor_read( DIST_SENSOR_VALS *val )
{
    int i;
    unsigned int dist;    // 16 bits 0..65535
    
#if 1
    dist = 0;
    for( i = 0; i < DIST_SENSOR_NUM_AVE; i++ )
    {
        //==============================================================
        // we will store the numbers used for the average,
        // so we can look at them later.
        // we will also add the left and right numbers to average them
        //==============================================================

        val->dist_ave[ i ] = analogRead( DIST_SENSOR );
        dist += val->dist_ave[ i ];
    }

    //==================================================================
    // now take the average.
    // The line "lf /= AVE;" is shorthand for "lf = lf / AVE;"
    //==================================================================

    dist /= DIST_SENSOR_NUM_AVE;

#else
    dist = analogRead( DIST_SENSOR );   // jus take one reading
#endif

    //==================================================================
    // now see if the average is above or below the threshold
    // if below, we are not at the box; above, at the box
    //==================================================================

    val->dist = dist;

    Serial.print( "dist = " );
    Serial.print( dist );
    Serial.print( "; thresh = " );
    Serial.print( val->thresh );
    Serial.println( "" );

    val->at_box = false;
    if( dist > val->thresh )
        val->at_box = true;
}



