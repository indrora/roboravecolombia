//======================================================================
// line_sensor.cpp
//
// These are the functions needed for the ping-pong arm
//
// void line_sensor_init()
// void line_sensor_read()
//
// read the two line sensors - left and right
//======================================================================

#include <Arduino.h>
#include "robot_pins.h"
#include "line_sensor.h"

//======================================================================
// init the pins to input
//======================================================================

void line_sensor_init()
{
    pinMode( LINE_LEFT_SENSOR, INPUT );
    pinMode( LINE_RIGHT_SENSOR, INPUT );

    pinMode( LINE_SENSOR_ENABLE, OUTPUT );      // H-bridge channels (A,B)
    digitalWrite( LINE_SENSOR_ENABLE, LOW );
}

//======================================================================
// There are two GP2Y0D810Z0F sensors - one on the left and one on the right.
//
// the sensor surface is roughly 3/8 inch from the track layout
// The sensor output = 0 when on white and mainly high for black
// To start with, we are going to sample every 1 milliSec for 100 milliSec
// and keep track of the number of times we are high
// then, if the number of times is .gt. a threshold, we have black, else white
//======================================================================

void line_sensor_read( LINE_SENSOR_VALS *val )
{
    int i;
    int lf_hi;
    int rt_hi;
    
    lf_hi = 0;
    rt_hi = 0;

    //==================================================================
    // enable the channel to power on the sensors
    // then need to wait 6 mSec
    //==================================================================

    digitalWrite( LINE_SENSOR_ENABLE, HIGH );
    delay( 6 );     // according to the datasheet

    //==================================================================
    // now sample the sensor outputs, with a 1 mSec delay per sample
    //==================================================================

    for( i = 0; i < LINE_SENSOR_NUM_SAMPLES; i++ )
    {
        //==============================================================
        // we will store the numbers used for the average,
        // so we can look at them later.
        // we will also add the left and right numbers to average them
        //==============================================================

        if( digitalRead( LINE_LEFT_SENSOR ) == HIGH )
            lf_hi++;

        if( digitalRead( LINE_RIGHT_SENSOR ) == HIGH )
            rt_hi++;

        delay( LINE_SENSOR_DELAY );     // in milliSec
    }

    //==================================================================
    // now turn the sensors off
    //==================================================================

    digitalWrite( LINE_SENSOR_ENABLE, LOW );
    delay( 10 );     // we need some time OFF - not clear from datasheet

    //==================================================================
    // now see if the average is above or below the threshold
    // if below, we see white; if above, we see black
    // we will assume we see white, then cgheck to see if really black
    //==================================================================

    val->left_sensor  = false;
    val->right_sensor = false;

#if 1

    Serial.print( "raw: lf,rt " );
    Serial.print( lf_hi );
    Serial.print( "," );
    Serial.print( rt_hi );
    Serial.print( ";   " );

#endif

    val->left_raw  = lf_hi;
    val->right_raw = rt_hi;

    if( lf_hi >= LINE_SENSOR_THRESH )
        val->left_sensor  = true;

    if( rt_hi >= LINE_SENSOR_THRESH )
        val->right_sensor  = true;
}


