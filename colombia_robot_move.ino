//======================================================================
// prototype sketch
//
// all robot actions will go here
//======================================================================


#include "parser.h"
#include "drive_motors.h"
#include "memory_free.h"


//======================================================================
// this is the user-settable speed
// see drive_motor_speed()
// we initialize it to the max speed
//======================================================================

byte user_speed = MOTOR_MAX_SPEED;

//======================================================================
// this is the table of user commands
// When the command is typed into the "send" box in the Serial Monitor,
// the table is searched for the command.
// When the command is found, the function in the PARSER_CALLBACK( function )
// is called. That function can then use the arguments for the action
//======================================================================

Command user_commands[] =
{
  //=============================================================
  // First item is the command name
  // second item is the number of arguments it takes.
  // third item is the name of the function to call.
  //=============================================================

  {"fwd",    1,  PARSER_CALLBACK(cmd_fwd),   "fwd <#sec> - move forward #sec"},
  {"back",   1,  PARSER_CALLBACK(cmd_back),  "back <#sec> - move back #sec"},
  {"left",   1,  PARSER_CALLBACK(cmd_left),  "left <#sec> - move left #sec"},
  {"right",  1,  PARSER_CALLBACK(cmd_right), "right <#sec> - move right #sec"},
  {"spinleft",   1,  PARSER_CALLBACK(cmd_spin_left),
                            "spin_left <#sec> - spin left #sec"},
  {"spinright",  1,  PARSER_CALLBACK(cmd_spin_right),
                            "spin_right <#sec> - move spin #sec"},

  {"drivespeed",  0,  PARSER_CALLBACK(cmd_drive_speed),
               "drivespeed [<speed>] - print speed or set speed 50..255"},

  //=============================================================
  // This tells us we have hit the end of our command list.
  // Don't touch this. Don't put anything after this (it will be ignored!)
  //=============================================================

  {null,-1,null,null}
};


//======================================================================
// The Serial.begin(9600) line must be in the setup() function
//======================================================================

void setup()
{
  Serial.begin(9600);

  //===================================================================
  // the drive motors make the robot move forward, back, left, right
  //===================================================================

  drive_motors_init();
}


//======================================================================
// These are the command callback functions
// they are called by the parser for a specific command
//======================================================================
//
// cmd_drive_speed()
//
// set the speed from 0 to 255.
// 0 is full stop (no motion at all for any command) and 255 is full speed
// we will make the minimum speed that can be set at 50 - really slow,
// but the robot will move
//======================================================================

void cmd_drive_speed( ARGS *arg_list )
{
  //====================================================================
  // if 0 arguments, then print out the drivespeed
  //====================================================================

  if( arg_list->num_arguments == 0 )
  {
    Serial.print( "drivespeed = " );
    Serial.println( user_speed );
    return;
  }

  //====================================================================
  // if an argument was given, it must be one number between
  // MOTOR_MIN_USER_SPEED and MOTOR_MAX_SPEED
  //====================================================================

  if(  (arg_list->num_arguments != 1)
    || (arg_list->values[ 0 ] < MOTOR_MIN_USER_SPEED)
    || (arg_list->values[ 0 ] > MOTOR_MAX_SPEED) )
  {
    Serial.print( "drivespeed must be a speed " );
    Serial.print( MOTOR_MIN_USER_SPEED );
    Serial.print( " to " );
    Serial.println( MOTOR_MAX_SPEED );
    return;
  }

  //====================================================================
  // start the motion, delay values[0] second (1000 milliSec), stop motor
  //====================================================================

  user_speed = arg_list->values[ 0 ];
  Serial.print( "drivespeed = " );
  Serial.println( user_speed );
}


//======================================================================
//======================================================================

void cmd_fwd( ARGS *arg_list )
{
  if( arg_list->num_arguments != 1 )
  {
    Serial.println( "fwd needs one argument: number of seconds" );
    return;
  }

  //====================================================================
  // start the motion, delay values[0] second (1000 milliSec), stop motor
  //====================================================================

  drive_motors_fwd( user_speed );
  delay( 1000 * arg_list->values[ 0 ] );   // delay values[0] seconds
  drive_motors_stop();
}


//======================================================================
//======================================================================

void cmd_back( ARGS *arg_list )
{
  if( arg_list->num_arguments != 1 )
  {
    Serial.println( "back needs one argument: number of seconds" );
    return;
  }

  //====================================================================
  // start the motion, delay values[0] second (1000 milliSec), stop motor
  //====================================================================

  drive_motors_back( user_speed );
  delay( 1000 * arg_list->values[ 0 ] );   // delay values[0] seconds
  drive_motors_stop();
}


//======================================================================
//======================================================================

void cmd_left( ARGS *arg_list )
{
  if( arg_list->num_arguments != 1 )
  {
    Serial.println( "left needs one argument: number of seconds" );
    return;
  }

  //====================================================================
  // start the motion, delay values[0] second (1000 milliSec), stop motor
  //====================================================================

  drive_motors_left( user_speed );
  delay( 1000 * arg_list->values[ 0 ] );   // delay values[0] seconds
  drive_motors_stop();
}


//======================================================================
//======================================================================

void cmd_right( ARGS *arg_list )
{
  if( arg_list->num_arguments != 1 )
  {
    Serial.println( "right needs one argument: number of seconds" );
    return;
  }

  //====================================================================
  // start the motion, delay values[0] second (1000 milliSec), stop motor
  //====================================================================

  drive_motors_right( user_speed );
  delay( 1000 * arg_list->values[ 0 ] );   // delay values[0] seconds
  drive_motors_stop();
}


//======================================================================
//======================================================================

void cmd_spin_left( ARGS *arg_list )
{
  if( arg_list->num_arguments != 1 )
  {
    Serial.println( "spin_left needs one argument: number of seconds" );
    return;
  }

  //====================================================================
  // start the motion, delay values[0] second (1000 milliSec), stop motor
  //====================================================================

  drive_motors_spin_left( user_speed );
  delay( 1000 * arg_list->values[ 0 ] );   // delay values[0] seconds
  drive_motors_stop();
}


//======================================================================
//======================================================================

void cmd_spin_right( ARGS *arg_list )
{
  if( arg_list->num_arguments != 1 )
  {
    Serial.println( "spin_right needs one argument: number of seconds" );
    return;
  }

  //====================================================================
  // start the motion, delay values[0] second (1000 milliSec), stop motor
  //====================================================================

  drive_motors_spin_right( user_speed );
  delay( 1000 * arg_list->values[ 0 ] );   // delay values[0] seconds
  drive_motors_stop();
}


//======================================================================
// "run_parser();" must be in the loop() function
//======================================================================

void loop()
{
    if( Serial.available() )
        run_parser();
  
    memory_free_report();
}

