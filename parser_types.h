/*
 * Parser types
 * @author morgan gangwere
 * @summary A basic set of types for the 2-arg assembler.
 */

#ifndef _PARSER_TYPES_H_
#define _PARSER_TYPES_H_
#include <Arduino.h>
#include "parser.h"

#define null NULL

// macro to create better callbacks
#define PARSER_CALLBACK(callback) ((void*(*)(ARGS*))callback)


//======================================================================
// Structs we need.
//
// Change this number to change to total maximum number of arguments
// for a command
//======================================================================

#define MAX_ARGUMENT_COUNT 8

//======================================================================
// all callback functions will get a pointer to this structure,
// filled in with whatever arguments are given after a command
//======================================================================

typedef struct
{
  byte num_arguments;
  int  values[MAX_ARGUMENT_COUNT];
} ARGS;

//======================================================================
// these macros make it easier to get the argument values.
// They assume the function declarations have a parameter of ARGS *arg_list,
// ie      void cmd_fwd( ARGS *arg_list )
//
// NOTE: This assumes MAX_ARGUMENT_COUNT is 8
//======================================================================

#define ARG_COUNT       (arg_list->num_arguments)

#define ARG_0           (arg_list->values[ 0 ])
#define ARG_1           (arg_list->values[ 1 ])
#define ARG_2           (arg_list->values[ 2 ])
#define ARG_3           (arg_list->values[ 3 ])
#define ARG_4           (arg_list->values[ 4 ])
#define ARG_5           (arg_list->values[ 5 ])
#define ARG_6           (arg_list->values[ 6 ])
#define ARG_7           (arg_list->values[ 7 ])

//======================================================================
// this is the format of a table entry, to define what a command is
//
// The comments use this table entry as the example
//   {"add",  2,  PARSER_CALLBACK(cmd_add), "Add two numbers"},
//======================================================================

typedef struct
{
  //====================================================================
  // this is the name of the command
  // In the example, this is the "add" string
  //====================================================================

  const char *opcode;

  //====================================================================
  // this is the expected number of arguments.
  // In the example, this is the number 2
  // Note this is the MINIMUM number of expected arguments
  // You can create a command that takes a minimum, but type MORE arguments
  //====================================================================

  int min_operand_count;

  //====================================================================
  // This is the name of the callback function
  // In the examole, this is PARSER_CALLBACK(cmd_add)
  // The function must defined in the same file as the table
  //      void cmd_add( ARGS *arg_list )
  //      {
  //      }
  //====================================================================

  void *(* callback)(ARGS*);

  //====================================================================
  // The general command "help" will print out the list of commands,
  // plus this string
  // In this example, the help string is "Add two numbers"
  //====================================================================

  const char *help_text;

} Command;

#endif // _PARSER_TYPES_H_

