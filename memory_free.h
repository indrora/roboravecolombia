//======================================================================
// memory_free.h
//
// return the ammount of free RAM
//======================================================================

#ifndef _MEMORY_FREE_H_
#define _MEMORY_FREE_H_

void memory_free_report();
int memory_free();

#endif // _MEMORY_FREE_H_

