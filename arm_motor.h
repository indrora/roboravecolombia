//======================================================================
// arm_motor.h
//
// These are the functions and defines needed for arm_motor.cpp
//
// void arm_motor_init()
// void arm_motor_stop()
// void arm_motor_fwd( byte speed )
// void arm_motor_back( byte speed )
//
// The arm motor is connected to the H-bridge chip
// Change the #defines to the proper PWM outputs on the Arduino
// This mapping allows the motors to be connected to the pins
// and changed if needed
// Each motor has two PWM pins: called A and B
// (we need names, and these will work)
// remember the left and right motors rotate in OPPOSITE directions
// to make the robot move in the same direction
//======================================================================
// The basic motor circuit is:
//
//    [H-bridge "A"] ----- <motor> ----- [H-bridge "B"]
//
// both A and B are set to the minimum level by drive_motors_init()
// to make the motor turn in one direction, keep A at MIN_SPEED,
// and set B at MAX_SPEED. To make the motor turn in the opposite direction,
// set A at MAX_SPEED and B at MIN_SPEED
//======================================================================

#ifndef _ARM_MOTOR_H_
#define _ARM_MOTOR_H_

//======================================================================
// The motor speed is controlled by writing an analog value 0..255
//======================================================================

#define ARM_MOTOR_MIN_SPEED     0
#define ARM_MOTOR_MAX_SPEED     255

#define ARM_MOTOR_MIN_USER_SPEED    50  // minimum user can set the speed

//======================================================================

void arm_motor_init();
void arm_motor_stop();
void arm_motor_fwd( byte speed );
void arm_motor_back( byte speed );

#endif // _ARM_MOTOR_H_
