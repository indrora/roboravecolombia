//=============================================================================
// parser_support.cpp
//
// parser support package for Arduino. This allows us to fill in get_line()
// and put_line()
// ============================================================================

#include <Arduino.h>
#include "parser_support.h"

//=============================================================================
// TODO: FIXME - this is pretty ugly
//=============================================================================

extern int rep_times;
extern boolean rep_forever;
extern boolean rep_just_called;
extern boolean stop_repeat_flag;

//=============================================================================
// index into the line buffer
// gets reset to 0 for each new input buffer
//=============================================================================

static int line_idx = 0;

//=============================================================================
// void PARSER_get_line(char* buff, int len)
//=============================================================================

void PARSER_get_line(char *buff, int len)
{
  //========================================================================
  // Spit out a prompt
  //========================================================================

  Serial.print("cmd> ");

  //========================================================================
  // While we are less than our maximum allowed size,
  //========================================================================

  while( line_idx < len )
  {
    //======================================================================
    // we need serial data. Keep waiting until we get serial data.
    //======================================================================

    if(Serial.available() > 0)
    {
      //====================================================================
      // read a character.
      // want to convert uppercase to lowercase
      //====================================================================

      char got_char = Serial.read();
      if( isupper( got_char ) )
          got_char = tolower( got_char );

      //====================================================================
      // is it a newline?
      //====================================================================

      if(got_char == ASCII_NL || got_char == ASCII_CR)
      {
        //==================================================================
        // Yep, it's newline. Spit out a newline/carraige return
        // Then, terminate our buffer NULL, so its a valid C string
        //==================================================================

        Serial.print((char)ASCII_NL);
        Serial.print((char)ASCII_CR);
        buff[ line_idx ] = 0x00;

        //==================================================================
        // Now we don't need to be here anymore. Return to whoever called us.
        //==================================================================

        return;
      }

      //====================================================================
      // is it backspace?
      // PuTTY defaultly sends an ASCII DEL (0x7F)
      // while HyperTerm and a few others
      // send an ASCI BS (0x8)
      // We handle both. All terminals should
      // (according to ASCII) back up a character
      // when sent an ASCII DEL (0x7f)
      //     check [      what we got is an ASCII BS or ASCII DEL  ]
      //           [we aren't at the beginning of the buffer]
      //====================================================================

      else if((got_char == ASCII_BKSP || got_char == ASCII_DEL))
      {
        if( line_idx > 0 )
        {
          //================================================================
          // Send our ASCII DEL to the other side.
          //
          // NEED: BS, SPACE, BS
          //================================================================

          Serial.print((char)ASCII_DEL);

          //================================================================
          // Clean up after ourselves.
          //================================================================

          buff[ line_idx ] = 0x00;

          //================================================================
          // And back up.
          //================================================================

           line_idx--;
        }
        else
        {
          //================================================================
          // TODO: What does this mean?
          //================================================================

          Serial.print((char)ASCII_BEL);
        }
      }

      //====================================================================
      // Otherwise, check if we're being given a legitimate ASCII character.
      // We don't want to handle accented characters here, so we just make sure
      // that we only get ASCII printables.
      //====================================================================
      else if(got_char >= ASCII_PRINT_LOWER && got_char <= ASCII_PRINT_UPPER)
      {
        Serial.print(got_char);
        buff[ line_idx ] = got_char;
         line_idx++;
      }
      else
      {
        Serial.print((char)ASCII_BEL);
      }
    }
  }
}



//=============================================================================
//=============================================================================

void PARSER_put_line(const char* content)
{
  Serial.println(content);
}

//==========================================================================

#include <stdarg.h>

#define PUT_FMT_TEMP_LEN    80      // 128

static char tmp[ PUT_FMT_TEMP_LEN ]; // resulting string limited to 128 chars

void PARSER_put_format(const char *fmt, ... )
{
        va_list args;
        va_start (args, fmt );
        vsnprintf(tmp,  PUT_FMT_TEMP_LEN , fmt, args);
        va_end (args);
        Serial.print(tmp);
}


//==========================================================================

void PARSER_reset_line( boolean reset_rep, char *buf, unsigned int buf_len )
{
    
  if( reset_rep )
  {
    rep_times   = 1;
    rep_forever = false;
    stop_repeat_flag = false;
  }

  rep_just_called = false;

  //==================================================================
  // now zero out the buffer for next command
  // TODO: replace with memset()
  //==================================================================

  for(unsigned int idx = 0; idx < buf_len; idx++ )
    buf[ idx ] = 0x00;

  line_idx = 0;
}



