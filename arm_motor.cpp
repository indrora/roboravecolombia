//======================================================================
// arm_motor.cpp
//
// These are the functions needed for the ping-pong arm
//
// void arm_motor_init()
// void arm_motor_stop()
// void arm_motor_fwd( byte speed )
// void arm_motor_back( byte speed )
//
// The arm motor are connected to the H-bridge chip
// Change the #defines to the proper PWM outputs on the Arduino
// This mapping allows the motors to be connected to the pins
// and changed if needed
// The motor has two PWM pins: called A and B
// (we need names, and these will work)
// there are NO limit switches
//======================================================================

#include <Arduino.h>
#include "robot_pins.h"
#include "arm_motor.h"

//======================================================================

void arm_motor_init()
{
  //====================================================================
  // set all PWM Motor pins to OUTPUT
  //====================================================================

  pinMode( ARM_MOTOR_A, OUTPUT );
  pinMode( ARM_MOTOR_B, OUTPUT );
  pinMode( ARM_MOTOR_ENABLE, OUTPUT );

  //====================================================================
  // set all H-bridge outputs to the MINIMUM value
  //====================================================================

  arm_motor_stop();
}

//======================================================================
// The basic motor circuit is:
//
//    [H-bridge "A"] ----- <motor> ----- [H-bridge "B"]
//
// both A and B are set to the minimum level by drive_motors_init()
// to make the motor turn in one direction, keep A at MIN_SPEED,
// and set B at MAX_SPEED. To make the motor turn in the opposite direction,
// set A at MAX_SPEED and B at MIN_SPEED
//
// later .. by seetting to MIN speed, the battery is connected to ground
// via a resistor in the h-bridge, draining the battery
// the real fix is to add three lines to explicitly enable the channels
//======================================================================

void arm_motor_stop()
{
  digitalWrite( ARM_MOTOR_ENABLE, LOW );
  analogWrite( ARM_MOTOR_A, ARM_MOTOR_MIN_SPEED );
  analogWrite( ARM_MOTOR_B, ARM_MOTOR_MIN_SPEED );
}

//======================================================================

void arm_motor_fwd( byte speed )
{
  digitalWrite( ARM_MOTOR_ENABLE, HIGH );
  analogWrite( ARM_MOTOR_A, speed );
}

//======================================================================

void arm_motor_back( byte speed )
{
  digitalWrite( ARM_MOTOR_ENABLE, HIGH );
  analogWrite( ARM_MOTOR_B, speed );
}




