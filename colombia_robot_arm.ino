//======================================================================
// prototype sketch - ARM motor example
//
// all robot actions will go here
//======================================================================


#include "parser.h"
#include "arm_motor.h"
#include "memory_free.h"


//======================================================================
// this is the user-settable speed
// see arm_motor_speed()
// we initialize it to the max speed
//======================================================================

byte arm_user_speed = ARM_MOTOR_MAX_SPEED;

//======================================================================
// this is the table of user commands
// When the command is typed into the "send" box in the Serial Monitor,
// the table is searched for the command.
// When the command is found, the function in the PARSER_CALLBACK( function )
// is called. That function can then use the arguments for the action
//======================================================================

Command user_commands[] =
{
  //=============================================================
  // First item is the command name
  // second item is the number of arguments it takes.
  // third item is the name of the function to call.
  //=============================================================

  {"armfwd",    1,  PARSER_CALLBACK(cmd_arm_fwd),
              "armfwd <#sec> - arm forward #milliSec"},
  {"armback",   1,  PARSER_CALLBACK(cmd_arm_back),
             "armback <#sec> - arm back #milliSec"},

  {"armspeed",  0,  PARSER_CALLBACK(cmd_arm_speed),
               "armspeed [<speed>] - print arm speed or set speed 50..255"},

  //=============================================================
  // This tells us we have hit the end of our command list.
  // Don't touch this. Don't put anything after this (it will be ignored!)
  //=============================================================

  {null,-1,null,null}
};


//======================================================================
// The Serial.begin(9600) line must be in the setup() function
//======================================================================

void setup()
{
  Serial.begin(9600);

  //===================================================================
  // the arm motor make the robot dump the ping-pong balls
  //===================================================================

  arm_motor_init();
}


//======================================================================
// These are the command callback functions
// they are called by the parser for a specific command
//======================================================================
//
// cmd_arm_speed()
//
// set the speed from 0 to 255.
// 0 is full stop (no motion at all for any command) and 255 is full speed
// we will make the minimum speed that can be set at 50 - really slow,
// but the robot will move
//======================================================================

void cmd_arm_speed( ARGS *arg_list )
{
  //====================================================================
  // if 0 arguments, then print out the armspeed
  //====================================================================

  if( arg_list->num_arguments == 0 )
  {
    Serial.print( "armspeed = " );
    Serial.println( arm_user_speed );
    return;
  }

  //====================================================================
  // if an argument was given, it must be one number between
  // ARM_MOTOR_MIN_USER_SPEED and ARM_MOTOR_MAX_SPEED
  //====================================================================

  if(  (arg_list->num_arguments != 1)
    || (arg_list->values[ 0 ] < ARM_MOTOR_MIN_USER_SPEED)
    || (arg_list->values[ 0 ] > ARM_MOTOR_MAX_SPEED) )
  {
    Serial.print( "armspeed must be a speed " );
    Serial.print( ARM_MOTOR_MIN_USER_SPEED );
    Serial.print( " to " );
    Serial.println( ARM_MOTOR_MAX_SPEED );
    return;
  }

  //====================================================================
  // start the motion, delay values[0] second (1000 milliSec), stop motor
  //====================================================================

  arm_user_speed = arg_list->values[ 0 ];
  Serial.print( "armspeed = " );
  Serial.println( arm_user_speed );
}


//======================================================================
// argument is number milliSec
//======================================================================

void cmd_arm_fwd( ARGS *arg_list )
{
  if( arg_list->num_arguments != 1 )
  {
    Serial.println( "armfwd needs one argument: number of milliSec" );
    return;
  }

  //====================================================================
  // start the motion, delay values[0] milliSec, stop motor
  //====================================================================

  arm_motor_fwd( arm_user_speed );
  delay( arg_list->values[ 0 ] );   // delay values[0] milliSec
  arm_motor_stop();
}


//======================================================================
// argument is number milliSec
//======================================================================

void cmd_arm_back( ARGS *arg_list )
{
  if( arg_list->num_arguments != 1 )
  {
    Serial.println( "armback needs one argument: number of milliSec" );
    return;
  }

  //====================================================================
  // start the motion, delay values[0] milliSec, stop motor
  //====================================================================

  arm_motor_back( arm_user_speed );
  delay( arg_list->values[ 0 ] );   // delay values[0] milliSec
  arm_motor_stop();
}


//======================================================================
// "run_parser();" must be in the loop() function
//======================================================================

void loop()
{
    if( Serial.available() )
        run_parser();
  
    memory_free_report();
}

