//======================================================================
// line_sensor.h
//
// These are the functions and defines needed for line_sensor.cpp
//
// void line_sensor_init()
// void line_sensor_read( LINE_SENSOR_VALS *vals );
//
// There are two GP2Y0D810Z0F sensors - one on the left and one on the right.
// they want to return 0 for white
//======================================================================

#ifndef _LINE_SENSOR_H_
#define _LINE_SENSOR_H_

//======================================================================
// this is the return from the line_sensor_read() function
// the raw values will be 0..100
// The flags will be true if the sensor sees the line or false for white
//======================================================================

typedef struct
{
    boolean     left_sensor;
    boolean     right_sensor;

    byte        left_raw;
    byte        right_raw;

#define LINE_SENSOR_NUM_SAMPLES     100
#define LINE_SENSOR_DELAY           1       // in miiliSec
#define LINE_SENSOR_THRESH          80      // below; white; above; black

} LINE_SENSOR_VALS;


//======================================================================

void line_sensor_init();
void line_sensor_read( LINE_SENSOR_VALS *vals );

#endif // _LINE_SENSOR_H_
